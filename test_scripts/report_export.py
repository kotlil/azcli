import json
from az.cli import az
import os, sys

subscription = ("27b2a9c9-89f6-44cf-b4ec-74df29dce1fd")

setsubscription = az(f'account set --subscription {subscription}')
exit_code, subshowname, logs = az(f'account show --subscription {subscription} --query "name"')

print ( subshowname )

exit_code, command_result, logs = az(f'costmanagement query --type "Usage" --timeframe "TheLastMonth" --scope "subscriptions/"{subscription}')

if exit_code == 0:
    print (command_result)
else:
    print (logs)