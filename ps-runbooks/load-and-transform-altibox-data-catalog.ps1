# Read input parameters
param([object]$WebhookData)
if($WebhookData) {
    $Parameters = ConvertFrom-Json -InputObject $WebhookData.RequestBody
}

# Define variables
$KeyVaultNameDL = "lit-np-kv-perm-datalake"
$KeyVaultEntryDL = "datalake-litnpadlsv2-key"
$KeyVaultNameAS = "lit-pd-kv-cube-browser"
$KeyVaultEntryASID = "as-reader-userid"
$KeyVaultEntryASPwd = "as-reader-password"
$StorageAccountName = "litnpadlsv2"
$FileSystemName = "altibox-data-catalog"
$LocalFileNameTables = "tables.csv"
$DLFileNameTables = "tables.csv"
$LocalFileNameColumns = "columns.csv"
$DLFileNameColumns = "columns.csv"
$LocalFileNameMeasures = "measures.csv"
$DLFileNameMeasures = "measures.csv"
$DLFolder = "v1"
$ASServer = "asazure://westeurope.asazure.windows.net/litpdasaltibox" #"asazure://westeurope.asazure.windows.net/litnpasaltibox"

# Login
"STEP 1) Login start"
$ServicePrincipalConnection = Get-AutomationConnection -Name "AzureRunAsConnection"
Connect-AzAccount -Tenant $ServicePrincipalConnection.TenantID `
    -ApplicationId $ServicePrincipalConnection.ApplicationID   `
    -CertificateThumbprint $ServicePrincipalConnection.CertificateThumbprint `
    -ServicePrincipal
"Login completed"

# Read credentials from KeyVaults (and prepare cerdentials object for AS)
"STEP 2) Keyvaults reading start"
# Read DataLake key entry and convert to string from secure string
$DatalakeCredential = Get-AzKeyVaultSecret -vaultName $KeyVaultNameDL -name $KeyVaultEntryDL
$SSPtrAcctKeySecure = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($DatalakeCredential.Secretvalue)
$AccountKey = [System.Runtime.InteropServices.Marshal]::PtrToStringBSTR($SSPtrAcctKeySecure)
# Read AS password entry and handle as secure string
$AnalysisServicesCredentialPwd = Get-AzKeyVaultSecret -vaultName $KeyVaultNameAS -name $KeyVaultEntryASPwd
$ASSecurePwd = $AnalysisServicesCredentialPwd.SecretValue
# Read AS user entry and convert to string from secure string
$AnalysisServicesCredentialUserID = Get-AzKeyVaultSecret -vaultName $KeyVaultNameAS -name $KeyVaultEntryASID
$SSPtrASSecureUsr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($AnalysisServicesCredentialUserID.SecretValue)
$ASUsr = [System.Runtime.InteropServices.Marshal]::PtrToStringBSTR($SSPtrASSecureUsr)
# Create PSCredentials object for AS login purposes below
$AnalysisServicesCredentials = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $ASUsr,$ASSecurePwd 
"Keyvaults reading completed"

# Read data from Analysis Services
"STEP 3) Reading from analysis services"
# Read and parse list of DBs (catalog_name) from AS server
$DBSchemaCatalogsRaw = Invoke-ASCmd -Server:$ASServer -Query:"SELECT [catalog_name] FROM `$SYSTEM.DBSCHEMA_CATALOGS" -ServicePrincipal -Credential $AnalysisServicesCredentials -TenantId "22ca942f-06c2-4f38-9407-0e447dedbb67"
$DBSchemaCatalogsXML = [xml]$DBSchemaCatalogsRaw
$DBSchemaCatalogs = $DBSchemaCatalogsXML.return.root.row.catalog_name

# Prepare output arrays
$OutputTables = @()
$OutputColumns = @()
$OutputMeasures = @()

# Query each DB
foreach ($DB in $DBSchemaCatalogs)  {

    "Processing DB $DB..."
    # Query DB for tables
    $TMSchemaTablesRaw = Invoke-ASCmd -Server:$ASServer -Database $DB -Query:"select * from `$SYSTEM.TMSCHEMA_TABLES WHERE not IsHidden" -ServicePrincipal -Credential $AnalysisServicesCredentials -TenantId "22ca942f-06c2-4f38-9407-0e447dedbb67"
    $TMSchemaTablesXML = [xml]$TMSchemaTablesRaw

    foreach ($Table in $($TMSchemaTablesXML.return.root.row)) {
        #"TableID: $($Table.ID) Name: $($Table.name), IsHidden: $($Table.ishidden), Description: $($Table.description)"
        $OutputTables += [pscustomobject] @{
            CubeName = $DB
            TableID = $($Table.ID)
            TableName = $($Table.name)
            Description = $($Table.description)
        }
    }

    # Query DB for columns
    $TMSchemaColumnsRaw = Invoke-ASCmd -Server:$ASServer -Database $DB -Query:"select * from `$SYSTEM.TMSCHEMA_COLUMNS WHERE not IsHidden" -ServicePrincipal -Credential $AnalysisServicesCredentials -TenantId "22ca942f-06c2-4f38-9407-0e447dedbb67"
    $TMSchemaColumnsXML = [xml]$TMSchemaColumnsRaw

    foreach ($Column in $($TMSchemaColumnsXML.return.root.row)) {
        #"TableID: $($Column.TableID), ColumnID: $($Column.ID), Name: $($Column.ExplicitName), Description: $($Column.Description)"
        $OutputColumns += [pscustomobject] @{
            CubeName = $DB
            TableID = $($Column.TableID)
            ColumnID = $($Column.ID)
            ColumnName = $($Column.ExplicitName)
            Description = $($Column.description)
        }
    }

    # Query DB for measures
    $MDSchemaMeasuresRaw = Invoke-ASCmd -Server:$ASServer -Database $DB -Query:"select * from `$SYSTEM.MDSCHEMA_MEASURES WHERE MEASURE_IS_VISIBLE" -ServicePrincipal -Credential $AnalysisServicesCredentials -TenantId "22ca942f-06c2-4f38-9407-0e447dedbb67"
    $MDSchemaMeasuresXML = [xml]$MDSchemaMeasuresRaw

    foreach ($Measure in $($MDSchemaMeasuresXML.return.root.row)) {
        $OutputMeasures += [pscustomobject] @{
            CubeName = $DB
            MeasureName = $($Measure.Measure_Name)
            Expression = $($Measure.Expression)
            Description = $($Measure.description)
        }
    }

} # foreach $DB
"Reading from analysis services completed"

# Generate local CSV files
"STEP 4) Create local CSV files start"
$OutputTables | Export-CSV -Path $LocalFileNameTables -NoTypeInformation -Encoding UTF8
$OutputColumns | Export-CSV -Path $LocalFileNameColumns -NoTypeInformation -Encoding UTF8
$OutputMeasures | Export-CSV -Path $LocalFileNameMeasures -NoTypeInformation -Encoding UTF8
"Create local CSV files completed"

# Create storage context for datalake
"STEP 5) Datalake connection start"
$DLContext = New-AzStorageContext -StorageAccountName $StorageAccountName -StorageAccountKey $AccountKey
"Datalake connection completed"

# Copy file from local to DL
"STEP 6) Copying files to Datalake start"
$DLPath = $DLFolder + "/" + $DLFileNameTables
New-AzDataLakeGen2Item -Context $DLContext -FileSystem $FileSystemName -Path $DLPath -Source $LocalFileNameTables -Force

$DLPath = $DLFolder + "/" + $DLFileNameColumns
New-AzDataLakeGen2Item -Context $DLContext -FileSystem $FileSystemName -Path $DLPath -Source $LocalFileNameColumns -Force 

$DLPath = $DLFolder + "/" + $DLFileNameMeasures
New-AzDataLakeGen2Item -Context $DLContext -FileSystem $FileSystemName -Path $DLPath -Source $LocalFileNameMeasures -Force 
"Copying files to Datalake completed"

# Remove local file
"STEP 7) Removing local files start"
Remove-Item $LocalFileNameTables
Remove-Item $LocalFileNameColumns
Remove-Item $LocalFileNameMeasures
"Removing local files completed"

# Callback to DF
"STEP 8) Callback to DF start"
# Check if parameter callBackUri (= if script was called from DF and is waiting for callback)
if ($Parameters.callBackUri) {
    $CallBackUri = $parameters.callBackUri

    # Check $error variable if any error happened in this script
    if ($error) {
        $body = [ordered]@{
        error = @{
            ErrorCode = "PowerShellScriptError"
            Message = "Error in PowerShell runbook, check runbook results in automation account."
        }
        statusCode = "400"
        }
    }
    else {
        $body = [ordered]@{
            statusCode = "200"
        }
    }
    $bodyJson = $body | ConvertTo-Json
}

if ($CallBackUri) {
    Invoke-WebRequest -Uri $callBackUri -Method POST -Body $bodyJson -ContentType "application/json" -Verbose -usebasicparsing
}
"Callback to DF completed"
