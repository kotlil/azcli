#!/bin/bash
#az login

filter1=$1
#filter2=$2

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

#OneDrive setup
OneDrive='/mnt/c/Users/kotunpet/OneDrive - TietoEVRY'  #change path if you want automatically save and share all output files
mkdir -p "$OneDrive"/invoicing/$month/
echo "OneDrive path: $OneDrive/invoicing/$month/"

scriptname="$0"
echo "$scriptname has started"
name=$(basename $0 .sh) #removed ./ and .sh from scriptname
echo "outputs will be saved in folder $name and $OneDrive"

#cleaning before start
#rm -rf ./$name/$month/"$name".csv
mkdir -p ./$name/$month/

file="./"$name"/"$month"/"$name"_"$filter1"_"$time".csv"

#tuukkas command
#for a in $(az ad group get-member-groups -g "HUS DataWS Prod AITeam Readers" -o tsv); do az ad group show --query "displayName" -g $a; done

mapfile -t gf < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"')
#mapfile -t getMemberGroups1 < <(az ad group get-member-groups -g "$filter1" -o tsv) #$(az ad group get-member-groups -g "HUS DataWS Prod AITeam Readers" -o tsv)
echo "group name(L0);group name(L1);group name(L2);group name(L3);" > "$file"
#mapfile -t getMemberGroups1 < <(az ad group get-member-groups --query "[].{Name:displayName}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"')
for x in "${gf[@]}"
do
echo "$x"
mapfile -t getMemberGroups1 < <(az ad group get-member-groups -g "$x" -o tsv);
    for y in "${getMemberGroups1[@]}"
    do 
    #echo "$y"
    mapfile -t displayName < <(az ad group show --query "displayName" -g $y -o tsv); 
    
            for b in "${displayName[@]}"
                do
                mapfile -t getMemberGroups2 < <(az ad group get-member-groups -g "$b" -o tsv);
                echo "$x;$b;$d;$displayName3;" >> "$file";
                #echo "$b" >> ./$name/$month/test_displayName.csv;
                #echo "get_member: $b";
                    for c in "${getMemberGroups2[@]}"
                    do
                    mapfile -t displayName2 < <(az ad group show --query "displayName" -g $c -o tsv);
                    #echo "objectId: $c";
                        for d in "${displayName2[@]}"
                            do
                            mapfile -t getMemberGroups3 < <(az ad group get-member-groups -g "$d" -o tsv);
                            echo "$x;$b;$d;$displayName3;" >> "$file";
                             #echo "$d" >> ./$name/$month/test_displayName2.csv;
                                for e in "${getMemberGroups3[@]}"
                                    do
                                    mapfile -t displayName3 < <(az ad group show --query "displayName" -g $e -o tsv);
                                    echo "$x;$b;$d;$displayName3;" >> "$file";
                                     #echo "$displayName3" >> ./$name/$month/test_displayName3.csv;
                                     echo "."
                                    done
                            done                                     
                    done
            done
    done
done

sort -u -r "$file" -o "$file"

#copy to OneDrive
cp "$file" "$OneDrive"/invoicing/$month/"$name"_"$filter1"_"$time".csv

echo "done"
#script status>>
printf "$name started at: $systemtime has been completed\n" >> executed_status_$month.txt
cp ./executed_status_$month.txt "$OneDrive"/invoicing/$month/

#additional copy function to DDB operations OneDrive
cp "$OneDrive"/invoicing/$month/* "$OneDrive"/Documents/"00 - Outputs"/Invoicing/$month/  # attach DDB operations "Documents" folder to your OneDrive account if you want automatically copy all in to proper location without any additional manuall action in the future.