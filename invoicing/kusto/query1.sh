#!/bin/bash

#https://docs.microsoft.com/en-us/cli/azure/ext/log-analytics/monitor/log-analytics?view=azure-cli-latest
mkdir ./dashboards

echo "login to hus account"
az login
#az account set --subscription 27b2a9c9-89f6-44cf-b4ec-74df29dce1fd

#az monitor log-analytics query -w 39839539-428a-4abd-a775-3727681828a4 --analytics-query "AzureActivity | summarize count() by bin(timestamp, 1h)" -t P3DT12H
todatetime1="$1" # 2020-10-16T12:07:00
todatetime2="$2" # 2020-10-19T09:47:00

az monitor log-analytics query --workspace 39839539-428a-4abd-a775-3727681828a4 --analytics-query "ContainerLog | where TimeGenerated > todatetime('2020-10-16T12:07:00') | where TimeGenerated <= todatetime('2020-10-19T09:47:00') | extend app = extractjson('$.app', LogEntry) | extend type = extractjson('$.type', LogEntry) | where type == 'AUDIT'and app startswith'pex360' | project LogEntry | count" #-t P3DT12H
#az monitor log-analytics query --workspace 39839539-428a-4abd-a775-3727681828a4 --analytics-query "ContainerLog | where TimeGenerated > todatetime('$todatetime1') | where TimeGenerated <= todatetime('$todatetime2') | extend app = extractjson('$.app', LogEntry) | extend type = extractjson('$.type', LogEntry) | where type == 'AUDIT'and app startswith'pex360' | project LogEntry | count" -o tsv #-t P3DT12H
az portal dashboard list > ./dashboards/hus_dashboard_list.json
az portal dashboard show --name "b020529a-8c2a-43fe-8e4e-c665d117d79f" --resource-group "dashboards" > ./dashboards/hus_dashboard_custom_logs.json
az logout

echo "login to eksote account"
az login 
az portal dashboard list > ./dashboards/eksote_dashboard_list.json
az portal dashboard show --name "850e228a-f142-489e-8882-8ba50c1cab37" --resource-group "dashboards" > ./dashboards/eksote_dashboard.json
az logout

echo "login to keusote account"
az login 
az portal dashboard list > ./dashboards/keusote_dashboard_list.json
az portal dashboard show --name "7a162f4e-cdb3-43e4-a05e-c3461bfda8a8" --resource-group "dashboards" > ./dashboards/keusote_dashboard.json
az logout

echo "login to kymsote account"
az login 
az portal dashboard list > ./dashboards/keusote_dashboard_list.json
az portal dashboard show --name "c1f0ac1f-3eae-466a-a7e5-a999b1b390c9" --resource-group "dashboards" > ./dashboards/kymsote_dashboard.json
az logout



#az monitor log-analytics query --analytics-query
#                               --workspace
#                               [--timespan]
#                               [--workspaces]