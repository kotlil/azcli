#https://stackabuse.com/reading-and-writing-json-to-a-file-in-python/
# import json

# data = {}
# data['people'] = []
# data['people'].append({
#     'name': 'Scott',
#     'website': 'stackabuse.com',
#     'from': 'Nebraska'
# })
# data['people'].append({
#     'name': 'Larry',
#     'website': 'google.com',
#     'from': 'Michigan'
# })
# data['people'].append({
#     'name': 'Tim',
#     'website': 'apple.com',
#     'from': 'Alabama'
# })

# with open('data.json', 'w') as outfile:
#     json.dump(data, outfile)


    # json.dumps(['foo', {'bar': ('baz', None, 1.0, 2)}])  
    # '["foo", {"bar": ["baz", null, 1.0, 2]}]'  
    #    # print(json.dumps("\"foo\bar")) 

#################################

# with open('data.json') as json_file:
#     data = json.load(json_file)
#     for p in data['people']:
#         print('Name: ' + p['name'])
#         print('Website: ' + p['website'])
#         print('From: ' + p['from'])
#         print('')

# import json

# with open('Sps.txt') as json_file:
#     data = json.load(json_file)j
#     for p in data.Name['Sps']:
#         print('Name: ' + p['name'])
#         print('')

import json  
#https://moonbooks.org/Articles/How-to-read-a-JSON-file-using-python-/

with open('AllGroups_.json') as json_data:
    data_dict = json.load(json_data)
    print(data_dict)   #conversion double quotes to single
    
    data_str = json.dumps(data_dict)
    print(data_str)

    data_dict_02 = json.loads(data_str)
    print(data_dict_02)

#https://www.geeksforgeeks.org/json-formatting-python/