from az.cli import az
#import subprocess
#import os, sys
import array  #https://stackoverflow.com/questions/59169997/how-to-keep-command-output-in-array-in-python
import json

TietoAllasGroups = open("TietoAllasGroups.json", "w")
AllGroups = open("AllGroups.json", "w")
Sps = open("Sps.json", "w")

#from azure.cli.core import get_default_cli

# AzResult = namedtuple('AzResult', ['exit_code', 'result_dict', 'log'])
#exit_code, result_dict, logs = az("group show -n test")
#exit_code, result_dict, logs = az('ad group member list --group hus-billing-qa-ad')

#login = az('login')

#exit_code, result_dict, logs = az('ad group member list --group "HUS Data Prod Storage Vihta Readers" --query "[].{name:displayName}"')
#exit_code, result_dict, logs = az('ad group list --query "[].{Name:displayName}"')
exit_code, command_result, logs = az('ad group list --query "[].{Name:displayName, objectId:objectId}[? contains(Name,`TietoAllas`)]"')

# On 0 (SUCCESS) print result_dict, otherwise get info from `logs`
if exit_code == 0:
    print (command_result, file=TietoAllasGroups)
        
else:
    print (logs)

exit_code, command_result2, logs = az('ad group list --query "[].{Name:displayName, objectId:objectId}"')

# On 0 (SUCCESS) print result_dict, otherwise get info from `logs`
if exit_code == 0:
    print (command_result2, file=AllGroups)
    #sys.stdout.close ("json.json", "w")
    
else:
    print (logs)

exit_code, command_result3, logs = az('ad sp list --all --query "[].{Name:displayName}"')

# On 0 (SUCCESS) print result_dict, otherwise get info from `logs`
if exit_code == 0:
    print (command_result3, file=Sps)
    #sys.stdout.close ("json.json", "w")
    
else:
    print (logs)
    