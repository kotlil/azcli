from az.cli import az
# on Success, the `error_code` is 0 and the result_dict contains the output
az("group list") # list return tuple (exit_code, result_dict, log)
az("group list")[0] # 0
az("group list")[1] # print result_dict
az("group list")[1][0]['id'] # enumerate the id of the first element in dictionary

# On Error, the `error_code` will be != 1 and the log is present
az("group show -n does-not-exsist") # list return tuple (exit_code, result_dict, log)
az("group show -n does-not-exsist")[0] # 3
az("group show -n does-not-exsist")[2] # print the log