[
    {'Name': 'integration-structured-webhook'
    },
    {'Name': 'Microsoft Stream Mobile Native'
    },
    {'Name': 'billing-databrics-test'
    },
    {'Name': 'datapipelines-df-deployer-sp-app'
    },
    {'Name': 'pex-events-api-app'
    },
    {'Name': 'prod-annukaila'
    },
    {'Name': 'Microsoft Teams Shifts'
    },
    {'Name': 'korona-osoitteet-sp-app'
    },
    {'Name': 'Intune DiagnosticService'
    },
    {'Name': 'Azure Cost Management XCloud'
    },
    {'Name': 'hus-billing-prod-ad'
    },
    {'Name': 'CosmosDB Dedicated Instance'
    },
    {'Name': 'NetworkTrafficAnalyticsService'
    },
    {'Name': 'Windows Azure Service Management API'
    },
    {'Name': 'pextestesbackups'
    },
    {'Name': 'Microsoft Azure Windows Virtual Machine Sign-in'
    },
    {'Name': 'Billing deploy Prod'
    },
    {'Name': 'OneProfile Service'
    },
    {'Name': 'Microsoft.MileIQ.RESTService'
    },
    {'Name': 'covid-registry-audit-forward-app'
    },
    {'Name': 'AzureContainerService'
    },
    {'Name': 'Microsoft To-Do'
    },
    {'Name': 'Microsoft.OfficeModernCalendar'
    },
    {'Name': 'Microsoft.MileIQ.Dashboard'
    },
    {'Name': 'Azure Notification Service'
    },
    {'Name': 'prod-janisalmi'
    },
    {'Name': 'OCaaS Experience Management Service'
    },
    {'Name': 'Microsoft Teams Partner Tenant Administration '
    },
    {'Name': 'OCaaS Worker Services'
    },
    {'Name': 'Microsoft Teams RetentionHook Service'
    },
    {'Name': 'intapps-api'
    },
    {'Name': 'ps-service-sp-kv-app'
    },
    {'Name': 'doccano-app'
    },
    {'Name': 'Microsoft Teams Mailhook'
    },
    {'Name': 'Microsoft Azure AD Identity Protection'
    },
    {'Name': 'Skype Teams Firehose'
    },
    {'Name': 'Microsoft Parature Dynamics CRM'
    },
    {'Name': 'integration-consent-api-test-client'
    },
    {'Name': 'Azure Advisor'
    },
    {'Name': 'Azure HDInsight Service'
    },
    {'Name': 'Device Registration Service'
    },
    {'Name': 'Common Data Service'
    },
    {'Name': 'Power BI Service'
    },
    {'Name': 'Networking-MNC'
    },
    {'Name': 'husdl-tu1-workspaces-aks-admin'
    },
    {'Name': 'ComplianceWorkbenchApp'
    },
    {'Name': 'husdltu1archive'
    },
    {'Name': 'Discovery Service'
    },
    {'Name': 'Microsoft Teams Graph Service'
    },
    {'Name': 'Skype Team Substrate connector'
    },
    {'Name': 'M365 License Manager'
    },
    {'Name': 'Microsoft Partner Center'
    },
    {'Name': 'Microsoft Intune Web Company Portal'
    },
    {'Name': 'O365 Customer Monitoring'
    },
    {'Name': 'PowerApps Service'
    },
    {'Name': 'Power Platform Global Discovery Service'
    },
    {'Name': 'rest_api_sp_for_sql_server'
    },
    {'Name': 'rare-diseases-aic-814-timo-aho-sp-app'
    },
    {'Name': 'Skype Presence Service'
    },
    {'Name': 'Dynamics Lifecycle services'
    },
    {'Name': 'Azure DNS Managed Resolver'
    },
    {'Name': 'ISV Portal'
    },
    {'Name': 'hema-aic-815-huslab-sp-app'
    },
    {'Name': 'Export to data lake'
    },
    {'Name': 'ACR-Tasks-Network'
    },
    {'Name': 'Azure Iot Hub Publisher App'
    },
    {'Name': 'HPC Cache Resource Provider'
    },
    {'Name': 'Dynamics CRM Online Administration'
    },
    {'Name': 'Azure Cosmos DB'
    },
    {'Name': 'AKS_DEPLOYMENT_OPERATOR_USER_PRINCIPAL'
    },
    {'Name': 'ConnectionsService'
    },
    {'Name': 'SharePoint Online Web Client Extensibility'
    },
    {'Name': 'appsautoprodlogstorage'
    },
    {'Name': 'Microsoft Teams Wiki Images Migration'
    },
    {'Name': 'Diagnostic Services Trusted Storage Access'
    },
    {'Name': 'Office Shredding Service'
    },
    {'Name': 'Microsoft Cognitive Services'
    },
    {'Name': 'MS-PIM'
    },
    {'Name': 'Microsoft Flow CDS Integration Service'
    },
    {'Name': 'Microsoft Cloud App Security'
    },
    {'Name': 'Azure AD Identity Protection'
    },
    {'Name': 'Microsoft Teams UIS'
    },
    {'Name': 'integration-vihta'
    },
    {'Name': 'Dynamics Data Integration'
    },
    {'Name': 'Data Migration Service'
    },
    {'Name': 'bcrquest-pipeline-sp-app'
    },
    {'Name': 'prod_aw_hema'
    },
    {'Name': 'Microsoft Visio Data Visualizer'
    },
    {'Name': 'pex360-uat-functions-sp-app'
    },
    {'Name': 'integrations-df-sp-app'
    },
    {'Name': 'pex360-audit-forward'
    },
    {'Name': 'integration-apotti-extracts'
    },
    {'Name': 'Microsoft Office 365 Portal'
    },
    {'Name': 'pex360-functions-sp-app'
    },
    {'Name': 'prod_dwdatalake'
    },
    {'Name': 'AzureSupportCenter'
    },
    {'Name': 'integration-healthweb'
    },
    {'Name': 'Microsoft Invoicing'
    },
    {'Name': 'ProjectWorkManagement'
    },
    {'Name': 'Teams User Engagement Profile Service'
    },
    {'Name': 'Windows Virtual Desktop ARM Provider'
    },
    {'Name': 'Power Platform Governance Services'
    },
    {'Name': 'Microsoft Graph Change Tracking'
    },
    {'Name': 'Microsoft Device Management Checkin'
    },
    {'Name': 'Microsoft Flow Portal'
    },
    {'Name': 'integration-elisa-liikkuvuusdata'
    },
    {'Name': 'Cortana at Work Bing Services'
    },
    {'Name': 'Service Bus MSI App'
    },
    {'Name': 'Call Recorder'
    },
    {'Name': 'Microsoft.DynamicsMarketing'
    },
    {'Name': 'Azure Media Services'
    },
    {'Name': 'integration-huskorona-altistetiedot'
    },
    {'Name': 'husdltu1omopss'
    },
    {'Name': 'dbx-audit-forward-lapps'
    },
    {'Name': 'yuri-test-ui'
    },
    {'Name': 'Microsoft To-Do'
    },
    {'Name': 'Microsoft Teams Settings Store'
    },
    {'Name': 'dev_maindatalake'
    },
    {'Name': 'Azure Media Service'
    },
    {'Name': 'husdltu1dls'
    },
    {'Name': 'prod-sla-poller'
    },
    {'Name': 'databricks-spark-ext-timo-aho'
    },
    {'Name': 'makudl-tu-app-db'
    },
    {'Name': 'Microsoft Container Registry'
    },
    {'Name': 'Substrate-FileWatcher'
    },
    {'Name': 'ACR-Tasks-Prod'
    },
    {'Name': 'Geneva Alert RP'
    },
    {'Name': 'Microsoft Azure Log Search Alerts'
    },
    {'Name': 'pseudo-service-sp-app'
    },
    {'Name': 'Domain Controller Services'
    },
    {'Name': 'Microsoft Intune Advanced Threat Protection Integration'
    },
    {'Name': 'Microsoft Modern Contact Master'
    },
    {'Name': 'integration-kafka-health-check'
    },
    {'Name': 'husdl-tu1-clarity-fa'
    },
    {'Name': 'Connectors'
    },
    {'Name': 'husdl-tu1-services-aks-admin'
    },
    {'Name': 'Teams Calling Meeting Devices Services'
    },
    {'Name': 'billing-data-factory'
    },
    {'Name': 'Federated Profile Service'
    },
    {'Name': 'husdltu1storage'
    },
    {'Name': 'Substrate Instant Revocation Pipeline'
    },
    {'Name': 'Power Query Online GCC-L2'
    },
    {'Name': 'Microsoft Intune AndroidSync'
    },
    {'Name': 'Azure Monitor Control Service'
    },
    {'Name': 'My Apps'
    },
    {'Name': 'Office 365 Import Service'
    },
    {'Name': 'Compute Usage Provider'
    },
    {'Name': 'patient-text-pseudo-sp-app'
    },
    {'Name': 'IC3 Long Running Operations Service'
    },
    {'Name': 'App Studio for Microsoft Teams'
    },
    {'Name': 'Power Query Online'
    },
    {'Name': 'Groupies Web Service'
    },
    {'Name': 'Microsoft.MileIQ'
    },
    {'Name': 'Microsoft Teams Targeting Application'
    },
    {'Name': 'Intune DeviceActionService'
    },
    {'Name': 'Cloudockit'
    },
    {'Name': 'bidatabricks-lapps-app'
    },
    {'Name': 'SalesInsightsWebApp'
    },
    {'Name': 'Microsoft Teams Retail Service'
    },
    {'Name': 'Windows Virtual Desktop Client'
    },
    {'Name': 'Atlas'
    },
    {'Name': 'prod-waltergronhol,'
    },
    {'Name': 'Access IoT Hub Device Provisioning Service'
    },
    {'Name': 'Azure Maps Resource Provider'
    },
    {'Name': 'Azure Smart Alerts'
    },
    {'Name': 'prod-patient-explorer'
    },
    {'Name': 'BranchConnectWebService'
    },
    {'Name': 'Data Export Service for Microsoft Dynamics 365'
    },
    {'Name': 'husdl-tu1-tf-infra-aks'
    },
    {'Name': 'initial-adl-to-biobank-analytics-db'
    },
    {'Name': 'My Staff'
    },
    {'Name': 'ps-service-sp-app'
    },
    {'Name': 'Microsoft Office Web Apps Service'
    },
    {'Name': 'probot-outbound-privateorgs-pipeline-sp-app'
    },
    {'Name': 'Microsoft Stream Portal'
    },
    {'Name': 'husdl-prod-infra-terraform'
    },
    {'Name': 'M365 App Management Service'
    },
    {'Name': 'huskubeSP-20180724161712'
    },
    {'Name': 'prod_extaaltobiobank'
    },
    {'Name': 'Managed Service'
    },
    {'Name': 'Azure Marketplace Container Management API'
    },
    {'Name': 'Microsoft Invitation Acceptance Portal'
    },
    {'Name': 'Reply-At-Mention'
    },
    {'Name': 'Azure AD Identity Governance - Entitlement Management'
    },
    {'Name': 'Azure AD Application Proxy'
    },
    {'Name': 'workspaces-aks-app'
    },
    {'Name': 'prod-file-manager-authentication'
    },
    {'Name': 'Azure AD Identity Governance'
    },
    {'Name': 'makudl-ansible'
    },
    {'Name': 'Azure Reserved Instance Application'
    },
    {'Name': 'prod-miikaleminen'
    },
    {'Name': 'prod-henrilaine'
    },
    {'Name': 'huslab-reporting-pipeline-uat'
    },
    {'Name': 'Conferencing Virtual Assistant'
    },
    {'Name': 'prod_insite'
    },
    {'Name': 'Azure Machine Learning Services'
    },
    {'Name': 'prod_bi_databricks'
    },
    {'Name': 'Azns AAD Webhook'
    },
    {'Name': 'integration-biopankki-core'
    },
    {'Name': 'prod_biopankkidatalake'
    },
    {'Name': 'huslab-daily-run-app'
    },
    {'Name': 'hus-billing-prod-infra-deployer'
    },
    {'Name': 'O365 LinkedIn Connection'
    },
    {'Name': 'Microsoft.SupportTicketSubmission'
    },
    {'Name': 'Office 365 Search Service'
    },
    {'Name': 'GatewayRP'
    },
    {'Name': 'Skype for Business Management Reporting and Analytics - Legacy'
    },
    {'Name': 'integration-copy-tool'
    },
    {'Name': 'apps-api'
    },
    {'Name': 'MCAPI Authorization Prod'
    },
    {'Name': 'Microsoft Customer Engagement Portal'
    },
    {'Name': 'Log Analytics API'
    },
    {'Name': 'Azure Classic Portal'
    },
    {'Name': 'Microsoft Forms'
    },
    {'Name': 'KaizalaActionsPlatform'
    },
    {'Name': 'PowerAppsService'
    },
    {'Name': 'integration-pilviharppi'
    },
    {'Name': 'Microsoft Teams Web Client'
    },
    {'Name': 'integration-consent-api-babyscreen-client'
    },
    {'Name': 'EnterpriseAgentPlatform'
    },
    {'Name': 'Bot Framework Dev Portal'
    },
    {'Name': 'IAM Supportability'
    },
    {'Name': 'logreader'
    },
    {'Name': 'Teams ACL management service'
    },
    {'Name': 'kubernetes-app'
    },
    {'Name': 'Microsoft Teams Bots'
    },
    {'Name': 'Office365 Zoom'
    },
    {'Name': 'Microsoft Intune Enrollment'
    },
    {'Name': 'pex360-prod-adl-sp-app'
    },
    {'Name': 'Microsoft Office Licensing Service'
    },
    {'Name': 'prod_aiheaddatalake'
    },
    {'Name': 'SharePoint Notification Service'
    },
    {'Name': 'prod-korona-laaturekisteri-api-app'
    },
    {'Name': 'Microsoft Flow CDS Integration Service TIP1'
    },
    {'Name': 'pex-uat-events-api-app'
    },
    {'Name': 'workspaces-aks-api'
    },
    {'Name': 'Microsoft.EventGrid'
    },
    {'Name': 'OneNote'
    },
    {'Name': 'Microsoft Stream Service'
    },
    {'Name': 'KustoService'
    },
    {'Name': 'Microsoft Intune IW Service'
    },
    {'Name': 'Microsoft Teams Services'
    },
    {'Name': 'IDMPRODOIG'
    },
    {'Name': 'prod-eetupursiainen'
    },
    {'Name': 'prod-tomthiel'
    },
    {'Name': 'Azure Spring Cloud Domain-Management'
    },
    {'Name': 'IAMTenantCrawler'
    },
    {'Name': 'integration-consent-api'
    },
    {'Name': 'Hyper-V Recovery Manager'
    },
    {'Name': 'prometheus'
    },
    {'Name': 'Azure Traffic Manager and DNS'
    },
    {'Name': 'prod_tiw'
    },
    {'Name': 'prod-adl-acl-manager'
    },
    {'Name': 'Domain Controller Services'
    },
    {'Name': 'prod-korona-laaturekisteri-client-app'
    },
    {'Name': 'Microsoft Office Licensing Service Agents'
    },
    {'Name': 'Microsoft Mobile Application Management Backend'
    },
    {'Name': 'SubstrateActionsService'
    },
    {'Name': 'Microsoft Intune'
    },
    {'Name': 'Azure Management Groups'
    },
    {'Name': 'Dual-write'
    },
    {'Name': 'Office 365 Enterprise Insights'
    },
    {'Name': 'AI Builder Authorization Service'
    },
    {'Name': 'Microsoft Seller Dashboard'
    },
    {'Name': 'testi'
    },
    {'Name': 'Microsoft Operations Management Suite'
    },
    {'Name': 'Skype for Business Application Configuration Service'
    },
    {'Name': 'MarketplaceAPI ISV'
    },
    {'Name': 'Azure Key Vault Managed HSM'
    },
    {'Name': 'Azure Kubernetes Service AAD Server'
    },
    {'Name': 'Azure AD Identity Governance Insights'
    },
    {'Name': 'pex-uat-client-app'
    },
    {'Name': 'Backup Management Service'
    },
    {'Name': 'pexuatesbackups'
    },
    {'Name': 'Azure Lab Services'
    },
    {'Name': 'prod_pacsimages'
    },
    {'Name': 'prod-events-api'
    },
    {'Name': 'Microsoft Teams AuditService'
    },
    {'Name': 'azure-cli-2019-03-11-20-34-30'
    },
    {'Name': 'Microsoft Azure Linux Virtual Machine Sign-In'
    },
    {'Name': 'pex360-lapp-la-reader-app'
    },
    {'Name': 'makudltuadls'
    },
    {'Name': 'Skype for Business Online'
    },
    {'Name': 'Office 365 Management APIs'
    },
    {'Name': 'Microsoft_Azure_Support'
    },
    {'Name': 'Sway'
    },
    {'Name': 'Azure SQL Database'
    },
    {'Name': 'Application Insights Configuration Service'
    },
    {'Name': 'husdltu1externalws'
    },
    {'Name': 'Microsoft.Azure.DomainRegistration'
    },
    {'Name': 'Microsoft Mobile Application Management'
    },
    {'Name': 'Microsoft App Access Panel'
    },
    {'Name': 'MicrosoftAzureADFulfillment'
    },
    {'Name': 'Graph Connector Service'
    },
    {'Name': 'AIGraphClient'
    },
    {'Name': 'husdltu1dls1'
    },
    {'Name': 'SubscriptionRP'
    },
    {'Name': 'Microsoft Whiteboard Services'
    },
    {'Name': 'Databricks Resource Provider'
    },
    {'Name': 'husdltu1raw'
    },
    {'Name': 'umaku-tu-umakutest-c22af9b4-ffbb-47ae-8dfb-baa2657f4f86'
    },
    {'Name': 'Microsoft Teams VSTS'
    },
    {'Name': 'prod_logdatalake'
    },
    {'Name': 'Microsoft Intune AAD BitLocker Recovery Key Integration'
    },
    {'Name': 'Office 365 Client Admin'
    },
    {'Name': 'services-aks-api'
    },
    {'Name': 'Skype for Business Name Dictionary Service'
    },
    {'Name': 'IC3 Gateway'
    },
    {'Name': 'Azure Credential Configuration Endpoint Service'
    },
    {'Name': 'husdltu1export'
    },
    {'Name': 'yuri-test-api'
    },
    {'Name': 'Centralized Deployment'
    },
    {'Name': 'prod-teijokonttila'
    },
    {'Name': 'Azure Multi-Factor Auth Client'
    },
    {'Name': 'Skype Teams Calling API Service'
    },
    {'Name': 'ASA Curation Web Tool'
    },
    {'Name': 'Microsoft Graph'
    },
    {'Name': 'Microsoft Device Management Enrollment'
    },
    {'Name': 'Compute Resource Provider'
    },
    {'Name': 'Office Change Management'
    },
    {'Name': 'Managed Service Identity'
    },
    {'Name': 'Azure API Management'
    },
    {'Name': 'Azure AD Identity Governance - User Management'
    },
    {'Name': 'Microsoft Teams'
    },
    {'Name': 'Network Watcher Acis Extension'
    },
    {'Name': 'Microsoft Azure Workflow'
    },
    {'Name': 'clarity-sp-app'
    },
    {'Name': 'husdl-tu1-services-aks-deployment'
    },
    {'Name': 'prod_finnprostata'
    },
    {'Name': 'Marketplace Api'
    },
    {'Name': 'Azure Data Factory'
    },
    {'Name': 'Azure Gallery RP'
    },
    {'Name': 'husdl-tu1-patientdata-xfer'
    },
    {'Name': 'prod_patientexplorer3'
    },
    {'Name': 'cressidaods-delta-pipeline'
    },
    {'Name': 'Directory and Policy Cache'
    },
    {'Name': 'Microsoft.EventHubs'
    },
    {'Name': 'Skype and Teams Tenant Admin API'
    },
    {'Name': 'intapps-app'
    },
    {'Name': 'Conference Auto Attendant'
    },
    {'Name': 'prod-anttilarjo'
    },
    {'Name': 'probot-sp-app'
    },
    {'Name': 'Media Analysis and Transformation Service'
    },
    {'Name': 'Microsoft Intune Checkin'
    },
    {'Name': 'ps-service-integrations-sp-app'
    },
    {'Name': 'Verifiable Credentials Issuer Service'
    },
    {'Name': 'AAD Terms Of Use'
    },
    {'Name': 'Yggdrasil'
    },
    {'Name': 'Intune CMDeviceService'
    },
    {'Name': 'backup-transfers-sp-app'
    },
    {'Name': 'prod_multilabdatalake'
    },
    {'Name': 'aiteam-df-deployer-sp-app'
    },
    {'Name': 'husdl-tu1-integrations-df'
    },
    {'Name': 'SharePoint Home Notifier'
    },
    {'Name': 'PushChannel'
    },
    {'Name': 'Metrics Monitor API'
    },
    {'Name': 'Demeter.WorkerRole'
    },
    {'Name': 'Microsoft.Relay'
    },
    {'Name': 'husdltu1import'
    },
    {'Name': 'Microsoft SharePoint Online - SharePoint Home'
    },
    {'Name': 'Office 365 Information Protection'
    },
    {'Name': 'MicrosoftTeamsCortanaSkills'
    },
    {'Name': 'Azure SQL Managed Instance to Microsoft.Network'
    },
    {'Name': 'Windows Store for Business'
    },
    {'Name': 'Microsoft Substrate Management'
    },
    {'Name': 'Power Platform Dataflows Common Data Service Client'
    },
    {'Name': 'python-adls'
    },
    {'Name': 'Azure Data Warehouse Polybase'
    },
    {'Name': 'Azure Resources Topology'
    },
    {'Name': 'husdl-tu1-tf-infra-msi'
    },
    {'Name': 'husdltu1backup'
    },
    {'Name': 'huskubeSP-20180724155127'
    },
    {'Name': 'Cortana at Work Service'
    },
    {'Name': 'husdltu1adls'
    },
    {'Name': 'Microsoft Teams Templates Service'
    },
    {'Name': 'AADPremiumService'
    },
    {'Name': 'AzureBackupReporting'
    },
    {'Name': 'ad_reader'
    },
    {'Name': 'AD Hybrid Health'
    },
    {'Name': 'pex-uat-functions-sp-app'
    },
    {'Name': 'integration-asiakaspalaute'
    },
    {'Name': 'maku-dl-ansible'
    },
    {'Name': 'On-Premises Data Gateway Connector'
    },
    {'Name': 'Microsoft Threat Protection'
    },
    {'Name': 'Microsoft Teams - Device Admin Agent'
    },
    {'Name': 'Microsoft Policy Insights Provider Data Plane'
    },
    {'Name': 'Microsoft Device Directory Service'
    },
    {'Name': 'Microsoft Intune Service Discovery'
    },
    {'Name': 'Microsoft Service Trust'
    },
    {'Name': 'file_uploader'
    },
    {'Name': 'Azure Security Insights'
    },
    {'Name': 'Azure Key Vault'
    },
    {'Name': 'O365 Demeter'
    },
    {'Name': 'Automated Call Distribution'
    },
    {'Name': 'Compute Artifacts Publishing Service'
    },
    {'Name': 'services-aks-app'
    },
    {'Name': 'Teams and Skype for Business Administration'
    },
    {'Name': 'DevilFish'
    },
    {'Name': 'Capacity '
    },
    {'Name': 'eduuni.fi'
    },
    {'Name': 'prod-pex-uat-datapipeline'
    },
    {'Name': 'prod_bidatalake'
    },
    {'Name': 'Windows Azure Application Insights'
    },
    {'Name': 'Billing'
    },
    {'Name': 'DeploymentScheduler'
    },
    {'Name': 'Azure Regional Service Manager'
    },
    {'Name': 'Data Classification Service'
    },
    {'Name': 'prod_cleverhealth'
    },
    {'Name': 'Azure SQL Database Backup To Azure Backup Vault'
    },
    {'Name': 'AzDevOpsHusProdSpn'
    },
    {'Name': 'AzureDnsFrontendApp'
    },
    {'Name': 'Dynamics Provision'
    },
    {'Name': 'husdl-tu1-ai-biobank-dbx-audit-forward'
    },
    {'Name': 'ResourceHealthRP'
    },
    {'Name': 'pex360-events-api-app'
    },
    {'Name': 'Microsoft Partner'
    },
    {'Name': 'AAD Request Verification Service - PROD'
    },
    {'Name': 'AzureDatabricks'
    },
    {'Name': 'Targeted Messaging Service'
    },
    {'Name': 'asmcontainerimagescanner'
    },
    {'Name': 'husdltu1infra'
    },
    {'Name': 'prod-pseudo-service-backup-df'
    },
    {'Name': 'Exchange Office Graph Client for AAD - Noninteractive'
    },
    {'Name': 'Azure AD Identity Governance - SPO Management'
    },
    {'Name': 'MicrosoftAzureActiveDirectoryIntegratedApp'
    },
    {'Name': 'AzureDataShare'
    },
    {'Name': 'Microsoft Discovery Service'
    },
    {'Name': 'Microsoft Teams User Profile Search Service'
    },
    {'Name': 'covid19-registry-audit-forward'
    },
    {'Name': 'Azure AD Notification'
    },
    {'Name': 'husdl-tu1-aiteam-df'
    },
    {'Name': 'Meru19 First Party App'
    },
    {'Name': 'M365 Admin Services'
    },
    {'Name': 'Azure DocKit'
    },
    {'Name': 'prod-antoniosthanellas'
    },
    {'Name': 'bcrquest-finngen-pipeline-sp-app'
    },
    {'Name': 'prod-michaelshamberger'
    },
    {'Name': 'CPIM Service'
    },
    {'Name': 'Windows Azure Security Resource Provider'
    },
    {'Name': 'Azure Search Management'
    },
    {'Name': 'MaintenanceResourceProvider'
    },
    {'Name': 'hus-billing-qa-ad'
    },
    {'Name': 'ohdsi_whiterabbit'
    },
    {'Name': 'Permission Service O365'
    },
    {'Name': 'Storage Resource Provider'
    },
    {'Name': 'Network Watcher'
    },
    {'Name': 'ReportReplica'
    },
    {'Name': 'ps-service-clarity-sp-app'
    },
    {'Name': 'Microsoft.SMIT'
    },
    {'Name': 'husdltu1share'
    },
    {'Name': 'SharePoint Notification Service'
    },
    {'Name': 'Microsoft Dynamics CRM Learning Path'
    },
    {'Name': 'Skype for Business'
    },
    {'Name': 'Microsoft Information Protection Sync Service'
    },
    {'Name': 'HagTuotanto'
    },
    {'Name': 'analytics-workspace-sp-app'
    },
    {'Name': 'husdl-tu1-df'
    },
    {'Name': 'Microsoft.ServiceBus'
    },
    {'Name': 'Exchange Office Graph Client for AAD - Interactive'
    },
    {'Name': 'Microsoft.Azure.ActiveDirectoryIUX'
    },
    {'Name': 'Graph explorer'
    },
    {'Name': 'ansible_automation'
    },
    {'Name': 'Office Enterprise Protection Service'
    },
    {'Name': 'Azure Cosmos DB Virtual Network To Network Resource Provider'
    },
    {'Name': 'Microsoft Flow Service'
    },
    {'Name': 'WindowsDefenderATP'
    },
    {'Name': 'PowerAI'
    },
    {'Name': 'pex-client-app'
    },
    {'Name': 'Azure Container Instance Service'
    },
    {'Name': 'pex-uat-adl-reader-sp-app'
    },
    {'Name': 'Azure Time Series Insights'
    },
    {'Name': 'AzureLockbox'
    },
    {'Name': 'OCPS Admin Service'
    },
    {'Name': 'Azure Storage'
    },
    {'Name': 'OCaaS Client Interaction Service'
    },
    {'Name': 'husdl-tu1-clarity-df'
    },
    {'Name': 'Compute Recommendation Service'
    },
    {'Name': 'Office Delve'
    },
    {'Name': 'prod_grafana'
    },
    {'Name': 'Microsoft Teams Chat Aggregator'
    },
    {'Name': 'Azure Multi-Factor Auth Connector'
    },
    {'Name': 'Azure DNS'
    },
    {'Name': 'Azure AD Domain Services Sync'
    },
    {'Name': 'MicrosoftAzureRedisCache'
    },
    {'Name': 'Office 365 Exchange Online'
    },
    {'Name': 'SharePoint Online Client'
    },
    {'Name': 'MicrosoftAzureActiveAuthn'
    },
    {'Name': 'Application Insights API'
    },
    {'Name': 'apotti-fhir-history-to-atlas'
    },
    {'Name': 'Microsoft Mixed Reality'
    },
    {'Name': 'Azure Iot Hub'
    },
    {'Name': 'SQLVMResourceProviderAuth'
    },
    {'Name': 'OfficeGraph'
    },
    {'Name': 'Cortana Runtime Service'
    },
    {'Name': 'Jarvis Transaction Service'
    },
    {'Name': 'OfficeClientService'
    },
    {'Name': 'Microsoft Fluid Framework Preview'
    },
    {'Name': 'probot-outbound-pipeline-sp-app'
    },
    {'Name': 'Microsoft Exchange Online Protection'
    },
    {'Name': 'husdl-tu1-omop-df'
    },
    {'Name': 'O365SBRM Service'
    },
    {'Name': 'Microsoft Teams Intelligent Workspaces Interactions Service'
    },
    {'Name': 'NFV Resource Provider'
    },
    {'Name': 'prod_hemadatalake'
    },
    {'Name': 'Office Scripts Service'
    },
    {'Name': 'prod_exthematologia'
    },
    {'Name': 'integration-noona'
    },
    {'Name': 'AAD App Management'
    },
    {'Name': 'Azure OSSRDBMS Database'
    },
    {'Name': 'M365DataAtRestEncryption'
    },
    {'Name': 'OneDrive Web'
    },
    {'Name': 'Skype Business Voice Directory'
    },
    {'Name': 'Microsoft Dynamics 365 Apps Integration'
    },
    {'Name': 'Azure HDInsight Cluster API'
    },
    {'Name': 'Microsoft AppPlat EMA'
    },
    {'Name': 'Bot Service CMEK Prod'
    },
    {'Name': 'prod_uiportal'
    },
    {'Name': 'MileIQ Admin Center'
    },
    {'Name': 'Azure Spring Cloud Resource Provider'
    },
    {'Name': 'umaku-adl-file-uploader'
    },
    {'Name': 'Office 365 Reports'
    },
    {'Name': 'Azure Machine Learning Singularity'
    },
    {'Name': 'maku_prod_maindatalake'
    },
    {'Name': 'Microsoft Azure Policy Insights'
    },
    {'Name': 'Domain Controller Services'
    },
    {'Name': 'umaku-tu-billing-27b2a9c9-89f6-44cf-b4ec-74df29dce1fd'
    },
    {'Name': 'pex360esbackups'
    },
    {'Name': 'Microsoft B2B Admin Worker'
    },
    {'Name': 'prod-anttirantala'
    },
    {'Name': 'husdl-tu1-zone-shared-share'
    },
    {'Name': 'Azure Maps'
    },
    {'Name': 'prod_doccano'
    },
    {'Name': 'maku-dl-datafactory'
    },
    {'Name': 'OfficeFeedProcessors'
    },
    {'Name': 'prod_dw_reader'
    },
    {'Name': 'config_test'
    },
    {'Name': 'husdl-tu1-korona-laaturekisteri'
    },
    {'Name': 'Microsoft Approval Management'
    },
    {'Name': 'husdltu1audit'
    },
    {'Name': 'YammerOnOls'
    },
    {'Name': 'husdltu1published'
    },
    {'Name': 'kubernetes-api'
    },
    {'Name': 'prod_maindatalake'
    },
    {'Name': 'Microsoft Azure Stream Analytics'
    },
    {'Name': 'Azure Data Lake'
    },
    {'Name': 'Domain Controller Services'
    },
    {'Name': 'husdltu1log'
    },
    {'Name': 'CMAT'
    },
    {'Name': 'pexesbackups'
    },
    {'Name': 'Microsoft Teams AuthSvc'
    },
    {'Name': 'Common Data Service User Management'
    },
    {'Name': 'databricks-datalake'
    },
    {'Name': 'Signup'
    },
    {'Name': 'Azure Container Registry'
    },
    {'Name': 'O365 Secure Score'
    },
    {'Name': 'O365Account'
    },
    {'Name': 'Meru19 MySQL First Party App'
    },
    {'Name': 'Microsoft.Azure.SyncFabric'
    },
    {'Name': 'tu1pseudoservicebackups'
    },
    {'Name': 'Microsoft Office Licensing Service vNext'
    },
    {'Name': 'pex360-uat-client-app'
    },
    {'Name': 'Windows Virtual Desktop'
    },
    {'Name': 'SPAuthEvent'
    },
    {'Name': 'pex-test-client-app'
    },
    {'Name': 'integration-huskorona-vaestotaso'
    },
    {'Name': 'Microsoft Teams Task Service'
    },
    {'Name': 'Azure Logic Apps'
    },
    {'Name': 'AKS_SECURITY_OPERATOR_USER_PRINCIPAL'
    },
    {'Name': 'StreamToSubstrateRepl'
    },
    {'Name': 'husdl-tu1-clarity-db'
    },
    {'Name': 'test-automation'
    },
    {'Name': 'pex-test-events-api-app'
    },
    {'Name': 'Dynamics 365 Business Central'
    },
    {'Name': 'Dynamic Alerts'
    },
    {'Name': 'Azure Backup NRP Application'
    },
    {'Name': 'pex360-uat-adl-reader-sp-app'
    },
    {'Name': 'Azure Data Factory'
    },
    {'Name': 'Cortana Experience with O365'
    },
    {'Name': 'Microsoft.ExtensibleRealUserMonitoring'
    },
    {'Name': 'omsagent-husdl-tu1-tf-infra-aks'
    },
    {'Name': 'Azure Analysis Services'
    },
    {'Name': 'Yammer'
    },
    {'Name': 'Microsoft Azure Authorization Private Link Provider'
    },
    {'Name': 'Internal_AccessScope'
    },
    {'Name': 'Marketplace Caps API'
    },
    {'Name': 'Microsoft Windows AutoPilot Service API'
    },
    {'Name': 'teams contacts griffin processor'
    },
    {'Name': 'Windows Azure Active Directory'
    },
    {'Name': 'AzureAutomationAADPatchS2S'
    },
    {'Name': 'makudl-tu-intapp-db'
    },
    {'Name': 'apps-app'
    },
    {'Name': 'husdl-tu1-tf-infra-aks-agentpool'
    },
    {'Name': 'Azure DevOps'
    },
    {'Name': 'Service Encryption'
    },
    {'Name': 'Common Data Service License Management'
    },
    {'Name': 'Azure ESTS Service'
    },
    {'Name': 'Microsoft.CustomProviders RP'
    },
    {'Name': 'Event Hub MSI App'
    },
    {'Name': 'Microsoft Intune SCCM Connector'
    },
    {'Name': 'Azure Service Fabric Resource Provider'
    },
    {'Name': 'husdl-tu1-ext-hema-omop-df'
    },
    {'Name': 'Teams Application Gateway'
    },
    {'Name': 'prod_hoitodatalake'
    },
    {'Name': 'Office 365 Configure'
    },
    {'Name': 'Microsoft Teams Web Client'
    },
    {'Name': 'husdltu1internalws'
    },
    {'Name': 'PowerApps-Advisor'
    },
    {'Name': 'Power BI Premium'
    },
    {'Name': 'husdl-tu1-homedialysis-gillie-app'
    },
    {'Name': 'O365 UAP Processor'
    },
    {'Name': 'Microsoft Device Management EMM API'
    },
    {'Name': 'Microsoft Exact Data Match Upload Agent'
    },
    {'Name': 'pex360uatesbackups'
    },
    {'Name': 'Microsoft Exact Data Match Service'
    },
    {'Name': 'Azure Spring Cloud DiagSettings App'
    },
    {'Name': 'prod_extbabyscreen'
    },
    {'Name': 'Microsoft Intune API'
    },
    {'Name': 'Power Platform Data Analytics'
    },
    {'Name': 'husdltu1dls1'
    },
    {'Name': 'Microsoft Azure App Service'
    },
    {'Name': 'clarity-df-deployer-sp-app'
    },
    {'Name': 'Microsoft.Azure.DataMarket'
    },
    {'Name': 'Azure Machine Learning'
    },
    {'Name': 'husdltu1staging'
    },
    {'Name': 'Microsoft Teams AadSync'
    },
    {'Name': 'prod-ad-server'
    },
    {'Name': 'BI-kuber-Eric-testSP-20190121131610'
    },
    {'Name': 'prod-anttimartikainen'
    },
    {'Name': 'integration-babyscreen'
    },
    {'Name': 'Bing'
    },
    {'Name': 'Azure Graph'
    },
    {'Name': 'prod_patientexplorer2'
    },
    {'Name': 'CABProvisioning'
    },
    {'Name': 'aciapi'
    },
    {'Name': 'integration-huskorona'
    },
    {'Name': 'umaku-tu-billing-df746ca9-c5cb-4aa8-9753-6114dcb6b1da'
    },
    {'Name': 'logdatafactory'
    },
    {'Name': 'DirectoryLookupService'
    },
    {'Name': 'Teams Approvals'
    },
    {'Name': 'OCPS Checkin Service'
    },
    {'Name': 'integration-husradu'
    },
    {'Name': 'ad-dbx-main-scim-test'
    },
    {'Name': 'integration-finngen'
    },
    {'Name': 'AIRS'
    },
    {'Name': 'prod-sergeihayryrnen'
    },
    {'Name': 'kubedashboard-husdl-tu1-tf-infra-aks'
    },
    {'Name': 'Azure Monitor Restricted'
    },
    {'Name': 'IPSubstrate'
    },
    {'Name': 'pex360-uat-events-api-app'
    },
    {'Name': 'SharePoint Online Client Extensibility'
    },
    {'Name': 'azure-shared-image-gallery-sp'
    },
    {'Name': 'Request Approvals Read Platform'
    },
    {'Name': 'husdl-tu1-datapipelines-df'
    },
    {'Name': 'Azure AD Identity Governance - Dynamics 365 Management'
    },
    {'Name': 'Azure SQL Virtual Network to Network Resource Provider'
    },
    {'Name': 'integration-teho-benchmarking'
    },
    {'Name': 'Outlook Online Add-in App'
    },
    {'Name': 'hema-aic-815-sp-app'
    },
    {'Name': 'Microsoft password reset service'
    },
    {'Name': 'Policy Administration Service'
    },
    {'Name': 'husdl-tu1-workspaces-aks-deployment'
    },
    {'Name': 'covid19-hki-share-sp-app'
    },
    {'Name': 'Microsoft Teams ADL'
    },
    {'Name': 'Microsoft People Cards Service'
    },
    {'Name': 'pex360-client-app'
    },
    {'Name': 'integration-koronapandemia'
    },
    {'Name': 'Microsoft Azure Batch'
    },
    {'Name': 'multilab-delta-pipeline'
    },
    {'Name': 'Microsoft Social Engagement'
    },
    {'Name': 'prod_medengine'
    },
    {'Name': 'hematology-automation'
    },
    {'Name': 'IDML Graph Resolver Service and CAD'
    },
    {'Name': 'Azure Portal'
    },
    {'Name': 'MS Teams Griffin Assistant'
    },
    {'Name': 'prod_geesidatalake'
    },
    {'Name': 'Office 365 SharePoint Online'
    },
    {'Name': 'Microsoft 365 Security and Compliance Center'
    }
]