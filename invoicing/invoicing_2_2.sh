#!/bin/bash
#az login

filter1=$1 #for filtering any of groups contains in example: "TietoAllasTutkimus", if is empty, it will read all existing groups one by one
filter2=$2 #for filtering Readers, Writers, if is empty, all 
#filter3=$3 #todo: for checking user names from list (TietoUsers) in all groups

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

#OneDrive setup
OneDrive='/mnt/c/Users/kotunpet/OneDrive - TietoEVRY'  #change path if you want automatically save and share all output files
mkdir -p "$OneDrive"/invoicing/$month/
echo "OneDrive path: $OneDrive/invoicing/$month/"

scriptname="$0"
echo "$scriptname has started"
name=$(basename $0 .sh) #removed ./ and .sh from scriptname
echo "outputs will be saved in folder $name and $OneDrive"

#cleaning output folder before start
#rm -rf ./$name/$month/output

outputpath="./"$name"/"$month"/outputs"
mkdir -p $outputpath

############################
############################

gf=0 #empty variable for group filter
gm=0 #empty variable for group members

rm -rf ./$name/$month/excluded
#rm -rf ./$name/$month/users*.csv

mkdir -p ./$name/$month/excluded

#mapfile and readarray are synonyms
#mapfile -t exclude < exclude  
#exclude=( "ext-sergey.hayrynen" )
#https://github.com/stedolan/jq/issues/370

#https://stackoverflow.com/questions/11426529/reading-output-of-a-command-into-an-array-in-bash

#jq filtering for service principals:
mapfile -t sps < <(az ad sp list --query "[].{Id:to_string(appOwnerTenantId), ObjectId:objectId,Name:displayName}[? ! contains(Name,'billing')&& contains(Id,'d1e1178f-7a71-4d1d-83d7-32ce2952a5a2') && ! contains(Name,'maku') && ! contains(Name,'Billing')|| contains(Id,'null')]" --all| jq '.[].ObjectId'|tr -d '"')

#loading exlusion list with name filter: example: TietoAllasTutkimus
mapfile -t gf < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"') # 

readarray -t exclude < exclude #loading exlusion list of users

#todo: if file already exist, skip this part of counting:
echo "count;group" > ./$name/$month/"$filter1"_users_count_after_exclusion_"$time".csv  #creates empty file with columns name

#
#uncoment section "while / done" bellow  if you want to load static list#
#

    # while IFS= read -r line; do  
    #     if [[ " ${gf[@]} " =~ " $line " ]]; then 
    #         echo "$line" >> ./$name/$month/existing_values_in_gf.json
    #     else 
    #         gf+=( "$line" )  #append non existing value into array
    #         #touch ./$name/$month/"$line".json  #create files also for empty groups
    #         #echo $line
    #     fi
    # done < TietoAllasTutkimus  #static list of TietoAllasTutkimus groups

echo "Start"
for x in "${gf[@]}"
do
    echo "$x"
    #echo "."
    mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName,objectId:objectId}"| jq '.[].Name'|tr -d '"') #OK and used in mapping #  
    afterExclusion=()  #new list of users after exlusion based on this var

    for y in "${gm[@]}"
    do    
        #echo "array inside lopp:$exclude"
        if [[ " ${exclude[@]} " =~ " $y " ]]; then 
            echo "$y" >> ./$name/$month/excluded/"$x"_excluded_users_"$time".json
        else 
            afterExclusion+=( "$y" ) #using for summarazing users in all groups after exclusion
            #echo "$y" >> ./$name/$month/"$x".json  #list of users for checking 
        fi

        #echo "group member:$y"
        #all tieto users in         
        #case "${exclude[@]}" in *"$y"*) echo "$y" >> ./$name/$month/"$x"found_users_by_case.json;; esac

        #create final file with group name and count
        #count how many group memebers "gm" in each group "gf"
        #
        
    done

    # echo $afterExclusion
    #count="$(wc -l ./$name/$month/*.json)" #old soluiton based on created files
    #printf "count;group\n$count" > ./$name/$month/export_test.csv  #old soluiton based on created files

    echo "${#afterExclusion[@]};$x" >> ./$name/$month/"$filter1"_users_count_after_exclusion_"$time".csv  #
    
    echo "${#gm[@]};$x" >> ./$name/$month/"$filter1"_users_count_before_exclusion_"$time".csv #works fine
    
done

#todo:
#in how many groups is TietoAllasTutkimus member:#
#https://stackoverflow.com/questions/45099570/bash-mapping-two-arrays/45101204   combining ouptus from two arrays

#loading members of necessary groups: in example "Readers" (all if $filter2 si empty):
mapfile -t allgroups < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$filter2')]"| jq '.[].Name'|tr -d '"') # 

echo "group;is a member of" > ./$name/$month/"$filter1"_is_member_of_"$filter2"_"$time".temp

gm_all_contains=()
for z in "${allgroups[@]}"
    do
    echo "mapping members of the group: $z"

    mapfile -t gm_all_contains < <(az ad group member list --group "$z" --query "[].{Name:displayName,objectId:objectId}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"')
    #az ad group member list --group "$z" --query "[].{Name:displayName,objectId:objectId}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"' >> "$filter1"_is_member_of_"$filter2"_"$time".temp
    
    echo "$gm_all_contains;$z" >> ./$name/$month/"$filter1"_is_member_of_"$filter2"_"$time".temp
    #echo "${#gm_all_contains[@]}" >> ./$name/$month/inside_loop_in_how_many_groups_is_a_member_$filter1.csv
    done
    #sed -i '/^;/d' ./$name/$month/"$filter1"is_member_of_list.csv  #remove all lines contains ";"
    #awk '!/^;/' ./$name/$month/"$filter1"is_member_of_list.cs 
sed -n '/^;/!p' ./$name/$month/"$filter1"_is_member_of_"$filter2"_"$time".temp > ./$name/$month/"$filter1"_is_member_of_"$filter2"_"$time".csv

# to do: ask where to save the output file...
cp ./$name/$month/"$filter1"_users_count_after_exclusion_"$time".csv "$OneDrive"/invoicing/$month/"$name"_"$filter1"_users_count_after_exclusion_"$time"_actual.csv

#cleaning
mkdir -p ./$name/$month/temp
mv ./$name/$month/*.temp ./$name/$month/temp
mv ./$name/$month/*.csv $outputpath

echo "done"
#script status>>
printf "$name started at: $systemtime has been completed\n" >> executed_status_$month.txt
cp ./executed_status_$month.txt "$OneDrive"/invoicing/$month/

#additional copy function to DDB operations OneDrive
cp "$OneDrive"/invoicing/$month/* "$OneDrive"/Documents/"00 - Outputs"/Invoicing/$month/  # attach DDB operations "Documents" folder to your OneDrive account if you want automatically copy all in to proper location without any additional manuall action in the future.

#rm -rf ./$name/$month/"$filter1"_is_member_of_"$filter2".temp

    #https://stackoverflow.com/questions/17208931/bash-remove-all-lines-beginning-with-p/17210943

    #echo "${#gm_all_contains[@]}" >> ./$name/$month/outside_loop_in_how_many_groups_is_a_member_$filter1.csv

# countgroupsinsidegroups=()
# for x in "${gf[@]}"
# do
# echo "level 1:$x"
#     for z in "${allgroups[@]}"
#     do
#     echo "level 2:$z"
#         for y in "${gm_all[@]}"
#         do  
#         echo "level 3:$y"  
#             if [[ " $y " =~ " $x " ]]; then
#             countgroupsinsidegroups+=( "$x" )
#             echo "$x found in: $z " >> "$x"found_in"$z"
#             fi
#     done
#         done

#     echo "${#afterExclusion[@]};$x" >> ./$name/$month/length_exclude  #error: need to get count for each group separately
#     echo "${#gm[@]};$x" >> ./$name/$month/length_gm  #works fine
#     echo "${#countgroupsinsidegroups[@]};$x" >> ./$name/$month/count_test
# done
#echo "${#countgroupsinsidegroups[@]};$z" >> count_test

########################################
#DIRS2=( "powershell" "jq" "azcopy")
#for i in "${DIRS2[@]}"
# do
 #if [ -d "$HOME/apps2/$i" ] ; then

#################### 
###second part if we have some list with groups , better funcion already done in the beggining
# cat ./TietoAllasTutkimus | while read line 
# do
# touch ./$name/$month/"$line".json  # to create file if there are not any members

#     for x in "${line[@]}";
#         do
#             echo "checking and appending if file doesn't exist: $x"        
#             mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') 
#         done
        
#     for y in "${gm[@]}"
#         do   
#             #https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist
#             grep -q -F "$y" ./$name/$month/"$x".json
#             if [ $? -ne 0 ]; then
#             echo "$y" >> ./$name/$month/"$x".json
#             fi
                                
#         done   

# done

                        ##mapfile -t gf < TietoAllasTutkimus #https://stackoverflow.com/questions/53091505/add-elements-to-an-existing-array-bash
                                        #https://kaijento.github.io/2017/03/19/bash-read-file-into-array/
                                        
                                        #https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/
                                        #https://stackoverflow.com/questions/1951506/add-a-new-element-to-an-array-without-specifying-the-index-in-bash
