#!/bin/bash

## once in a while is needed to manually check users in each group if there are some new Tieto users and if so...add them to "./exclude" file list and re-run script after this update.
# files location example: "/2021-02/workdir/TietoAllasTutkimus HRULAB2.csv" - there is a lot of users, but last time was "Ext Mika Mattila" found in "/2021-02/workdir/TietoAllasTutkimus Finalk.csv"
#


#az login

#input parameters for comparing example: ./commands.sh 2020-07 2020-06

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

#OneDrive setup
OneDrive='/mnt/c/Users/kotunpet/OneDrive - TietoEVRY'  #change path if you want automatically save and share all output files
mkdir -p "$OneDrive"/invoicing/$month/

echo "OneDrive path: $OneDrive/invoicing/$month/"

scriptname="$0"
echo "$scriptname has started"
name=$(basename $0 .sh) #removed ./ and .sh from scriptname
echo "outputs will be saved localy in folder $name and remotely in $OneDrive"

#namefilter=$1 todo> 
actualmonth=$2
previousmonth=$3

outputpath="./"$name"/"$month"/outputs"
workdir="./"$name"/"$month"/workdir"

#preparing folders
echo "cleaning and preparing all folders:"
#rm -rf ./"$name"/"$month"
mkdir -p $outputpath #mkdir -p ./$month/output  
mkdir -p $workdir

#TietoAllasTutkimus users:
echo "operation in progress:"
cat ./TietoAllasTutkimus | while read line  #todo> groupname input parametr
do
    #usersingroups=$(az ad group member list --group "$line" --query "[].{name:displayName}" -o tsv > ./$month/"$line".csv) #az ad group member list --group 'TietoAllasTutkimus Biopankki' --query "[].{name:displayName}" -o tsv
    az ad group member list --group "$line" --query "[].{name:displayName}" -o tsv > $workdir/"$line".csv
    echo "loading: $line"
done

#backup files before exlusion
mkdir $workdir/backup
cp $workdir/* $workdir/backup/  #backup files

#exclude names from the list   
#https://stackoverflow.com/questions/5410757/how-to-delete-from-a-text-file-all-lines-that-contain-a-specific-string
#https://askubuntu.com/questions/76808/how-do-i-use-variables-in-a-sed-command

#Use double quotes to make the shell expand variables while preserving whitespace:
#sed -i "s/$var1/ZZ/g" "$file"

cat ./exclude | while read line
do 
    sed -i "/$line/d" $workdir/*.csv  # sed -i.bak '/Sys/d' ./$month/*.csv
    echo "excluding: $line"
done

#todo: remove duplicities in files


#sum of users in each group:
echo "sumarazing users..."
count="$(wc -l $workdir/*.csv)"
printf "count;group\n$count" > $outputpath/"$name"_TietoAllas_users_count_after_exclusion_"$time".csv
#replacing strings in output and doing output backup:
sed -i.bak "s/\ .\/$name\/$month\/workdir\//;/g" $outputpath/"$name"_TietoAllas_users_count_after_exclusion_"$time".csv #sed -i.bak 's/\ .\/2020-08\//;/g' "export_2020-08.csv"
sed -i 's/.csv//g' $outputpath/"$name"_TietoAllas_users_count_after_exclusion_"$time".csv #sed -i.bak 's/.csv//g' "export_2020-08.csv"
#unoconv --format xls $outputpath/export_"$month".csv

#to do: user input : path
#copy output to OneDrive
cp $outputpath/"$name"_TietoAllas_users_count_after_exclusion_"$time".csv "$OneDrive"/invoicing/$month/"$name"_TietoAllas_users_count_after_exlusion_"$time"_static_list.csv


#additional copy function to DDB operations OneDrive
cp "$OneDrive"/invoicing/$month/* "$OneDrive"/Documents/"00 - Outputs"/Invoicing/$month/  # attach DDB operations "Documents" folder to your OneDrive account if you want automatically copy all in to proper location without any additional manuall action in the future.

#endsystemtime=`date +"%Y %m %d %H:%M"`
#script status>>
printf "$name started at: $systemtime has been completed\n" >> executed_status_$month.txt
cp ./executed_status_$month.txt "$OneDrive"/invoicing/$month/

echo "comparing with previous month"
#comparing output folders:
#diff -r ./"$name"/"$actualmonth"/ ./"$name"/"$previousmonth"/ > $outputpath/diff_"$actualmonth"_"$previousmonth".txt #diff ./2020-07/TietoAllasTutkimus\ Biopankki.csv ./2020-07\ copy/TietoAllasTutkimus\ Biopankki.csv

#moving backup files:
#echo "moving backup files"
#mkdir ./$month/backup
#mv ./$month/*.bak ./$month/backup

#showing output on screen:
cat $outputpath/diff_"$actualmonth"_"$previousmonth".txt
cat $outputpath/export_"$month".csv

#databricks users:
echo "bidatabricks:" 
echo "https://adb-7644984000139018.18.azuredatabricks.net"
echo "token:" 
echo "dapi3373d3e92aaeafaff3e33077c748a72b"
/home/kotunpet/.local/bin/databricks configure --token
mkdir ./$name/$month/bidatabricks
databricks groups list-members --group-name Biopankki > ./$name/$month/bidatabricks/Biopankki.csv
databricks groups list-members --group-name AI > ./$name/$month/bidatabricks/AI.csv

#databricks --help
#databricks workspace
#databricks workspace ls

#ls /databricks-datasets >> ./$month/bidatabricks/datasets.csv

#adding user's sum at the end of the file:
grep -c "user_name" ./$name/$month/bidatabricks/Biopankki.csv >> ./$name/$month/bidatabricks/Biopankki.csv
grep -c "user_name" ./$name/$month/bidatabricks/AI.csv >> ./$name/$month/bidatabricks/AI.csv

#exclude lines 
#cat ./exclude | while read line
#do 
#sed -i.bak '/'$line'/d' ./$month/bidatabricks/Biopankki.csv  #backup file and direclty modify # sed -i.bak '/Sys/d' ./$month/*.csv
#echo "excluding: $line"
#done

#cat ./exclude | while read line
#do 
#sed -i.bak '/'$line'/d' ./$month/bidatabricks/AI.csv  #backup file and direclty modify # sed -i.bak '/Sys/d' ./$month/*.csv
#echo "excluding: $line"
#done

#another commands:
#az ad group member list --group 'HUS App Prod Korona-laaturekisteri App Users' --query "[].{name:displayName}" -o tsv
#az ad user list --query "[].{name:displayName,mail:mail}" -o tsv
#az ad group list --query "[].{name:displayName,objectID:objectId}" -o tsv
