#!/bin/bash

#az login

#map=$(az ad group list --query "[].{Name:displayName}[? contains(Name,'HUS Data Prod Storage Ravioli W')]"| jq '.[].Name'|tr -d '"')
#groups=$(az ad group list| jq 'map({"Id": .objectId, "Name": .displayName})') #> ./groups_mapping.json
#echo "$groups"
#echo "$map"

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

#OneDrive setup
OneDrive='/mnt/c/Users/kotunpet/OneDrive - TietoEVRY'  #change path if you want automatically save and share all output files
mkdir -p "$OneDrive"/invoicing/$month/
echo "OneDrive path: $OneDrive/invoicing/$month/"

scriptname="$0"
echo "$scriptname has started"
name=$(basename $0 .sh) #removed ./ and .sh from scriptname
echo "outputs will be saved in folder $name and $OneDrive"

#cleaning output folder before start
#rm -rf ./$name/$month/output

outputpath="./"$name"/"$month"/outputs"
mkdir -p $outputpath

############################
############################

#rm -rf MembersInAllgroups   #un-comment this block before first run 
#todo: check if files exist... then skip these az ad group list commands

mkdir -p ./$name/$month/MembersInAllgroups


az ad group list --query "[].{name:displayName,objectId:objectId}" > ./$name/$month/AllgroupsObjectIds.json #all groups with objectIds
az ad group list --query "[].{name:displayName}" -o tsv > ./$name/$month/AllGroups  #all groups list

#find TietoAllasTutkimus actual groups and create list:
az ad group list --query "[].{Name:displayName}[? contains(Name,'TietoAllas')]"| jq '.[].Name'|tr -d '"' > ./$name/$month/TietoAllas_actual

#list of all actual Service Principals
az ad sp list --all --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"' > ./$name/$month/SPS_actual

#azure users:  

#un-comment this block for the first time : todo. create options in case 1, case 2
echo "operation in progress:"
    cat ./$name/$month/AllGroups | while read line
    do
    az ad group member list --group "$line" --query "[].{name:displayName}" -o tsv > ./$name/$month/MembersInAllgroups/"$line".csv
    echo "loading: $line"
    done
    
        examples:
        usersingroups=$(az ad group member list --group "$line" --query "[].{name:displayName}" -o tsv > ./$month/"$line".csv) 
        az ad group member list --group 'TietoAllasTutkimus Biopankki' --query "[].{name:displayName}" -o tsv

#check all groups across all groups:
printf "is a member of;group\n" > ./$name/$month/$name"_GroupsInsideGroups_$time".csv
find ./$name/$month/MembersInAllgroups/ -type f -exec grep -Hf ./$name/$month/AllGroups {} + >> ./$name/$month/$name"_GroupsInsideGroups_$time".csv
sed -i 's/.\/'$name'\/'$month'\/MembersInAllgroups\///g' ./$name/$month/$name"_GroupsInsideGroups_$time".csv
sed -i 's/.csv:/;/g' ./$name/$month/$name"_GroupsInsideGroups_$time".csv

#check all tieto users in all groups:
printf "is a member of;user\n" > ./$name/$month/$name"_TietoUsersInGroups_$time".csv
find ./$name/$month/MembersInAllgroups/ -type f -exec grep -Hf ./TietoUsers {} + >> ./$name/$month/$name"_TietoUsersInGroups_$time".csv
sed -i 's/.\/'$name'\/'$month'\/MembersInAllgroups\///g' ./$name/$month/$name"_TietoUsersInGroups_$time".csv
sed -i 's/.csv:/;/g' ./$name/$month/$name"_TietoUsersInGroups_$time".csv

#check all "service principals" users in all groups:
printf "is a member of;SP\n" > ./$name/$month/$name"_SPSInGroups_"$time.csv
find ./$name/$month/MembersInAllgroups/ -type f -exec grep -Hf ./$name/$month/SPS_actual {} + >> ./$name/$month/$name"_SPSInGroups_"$time.csv
sed -i 's/.\/'$name'\/'$month'\/MembersInAllgroups\///g' ./$name/$month/$name"_SPSInGroups_"$time.csv
sed -i 's/.csv:/;/g' ./$name/$month/$name"_SPSInGroups_"$time.csv

#check all "specified" users in all groups:
printf "is a member of;user\n" > ./$name/$month/$name"_specifiedUsersInGroups_"$time.csv
find ./$name/$month/MembersInAllgroups/ -type f -exec grep -Hf ./specifiedUsers {} + >> ./$name/$month/$name"_specifiedUsersInGroups_"$time.csv
sed -i 's/.\/'$name'\/'$month'\/MembersInAllgroups\///g' ./$name/$month/$name"_specifiedUsersInGroups_"$time.csv
sed -i 's/.csv:/;/g' ./$name/$month/$name"_specifiedUsersInGroups_"$time.csv

# check all TietoAllasTutkimus groups in all groups:
# printf "is a member of;user\n" > ./$name/$month/CressidaODSInGroups.csv
# find ./$name/$month/MembersInAllgroups/ -type f -exec grep -Hf ./$name/$month/CressidaODS {} + >> ./$name/$month/TietoAllasTutkimusInGroups.csv
# sed -i 's/.\/'$name'\/MembersInAllgroups\///g' ./$name/$month/CressidaODSInGroups.csv
# sed -i 's/.csv:/;/g' ./$name/$month/CressidaODSInGroups.csv

mv ./$name/$month/*.csv ./$outputpath

echo "done"
#script status>>
printf "$name started at: $systemtime has been completed\n" >> executed_status_$month.txt

#copy final files to OneDrive:
cp ./$outputpath/$name"_SPSInGroups_"$time.csv "$OneDrive"/invoicing/$month/$name"_SPSInGroups_"$time.csv
cp ./$outputpath/$name"_specifiedUsersInGroups_"$time.csv "$OneDrive"/invoicing/$month/$name"_specifiedUsersInGroups_"$time.csv
cp ./executed_status_$month.txt "$OneDrive"/invoicing/$month/ # Copy to Your OneDrive account (TietoEvry's one)

#additional copy function to DDB operations OneDrive
cp "$OneDrive"/invoicing/$month/* "$OneDrive"/Documents/"00 - Outputs"/Invoicing/$month/  # attach DDB operations "Documents" folder to your OneDrive account if you want automatically copy all in to proper location without any additional manuall action in the future.

#groupstsv=$(az ad group list| jq 'map({"Id": .objectId, "Name": .displayName}) -o tsv') > ./groups_mappingtsv

#jq ./"$groups"_mapping.json

#echo "$map"
#todo : json to csv

#TietoallasTutkimus BI

#dataset ml
#https://docs.microsoft.com/en-us/azure/machine-learning/how-to-manage-workspace-cli

#az ml folder attach -w myworkspace -g myresourcegroup
#az ml folder attach -w 
#az ml dataset list
#az ml workspace list

#az ad app list

#databricks workspace ls
#databricks workspace ls --profile <profile>

#https://docs.microsoft.com/en-us/azure/databricks/dev-tools/databricks-utils

#%fs ls /databricks-datasets

#https://docs.microsoft.com/en-us/azure/databricks/data/databricks-datasets

#https://docs.microsoft.com/en-us/cli/azure/ad?view=azure-cli-latest

