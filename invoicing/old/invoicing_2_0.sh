#!/bin/bash
#az login
name="invoicing_2_0"
filter1=$1
filter2=$2

gf=0 #group filter
gm=0 #group members
#rm -rf ./$name

mkdir -p ./$name/excluded

#mapfile and readarray are synonyms
#mapfile -t exclude < exclude  
#exclude=( "ext-sergey.hayrynen" )
#https://github.com/stedolan/jq/issues/370

#https://stackoverflow.com/questions/11426529/reading-output-of-a-command-into-an-array-in-bash

#jq filtering for service principals:
mapfile -t sps < <(az ad sp list --query "[].{Id:to_string(appOwnerTenantId), ObjectId:objectId,Name:displayName}[? ! contains(Name,'billing')&& contains(Id,'d1e1178f-7a71-4d1d-83d7-32ce2952a5a2') && ! contains(Name,'maku') && ! contains(Name,'Billing')|| contains(Id,'null')]" --all| jq '.[].ObjectId'|tr -d '"')

#loading exlusion list with name filter: example: TietoAllasTutkimus
mapfile -t gf < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$filter1')]"| jq '.[].Name'|tr -d '"') # 

readarray -t exclude < exclude #loading exlusion list of users


#appending list of all TietoAllasTutkimus groups from static file:
#todo: check if group name is already in array, if not, append to new var and use new var later...

    # while IFS= read -r line; do
    #        gf+=( "$line" )
    #        #touch ./$name/"$line".json  #create files also for empty groups
    #        #echo $line
    # done < TietoAllasTutkimus

    #mapfile -t gf < TietoAllasTutkimus #https://stackoverflow.com/questions/53091505/add-elements-to-an-existing-array-bash
                                        #https://kaijento.github.io/2017/03/19/bash-read-file-into-array/
                                        
                                        #https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/
                                        #https://stackoverflow.com/questions/1951506/add-a-new-element-to-an-array-without-specifying-the-index-in-bash

#data operations with data.json
#jq --slurp -f data.json < $gf  # jq --slurp -f data.json < test.json

#echo $gf

echo "Start"
for x in "${gf[@]}"
do
    echo "$x"
    #echo "."
    mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName,objectId:objectId}"| jq '.[].Name'|tr -d '"') #OK and used in mapping #  
    afterExclusion=()  #new list of users after exlusion based on this var

    for y in "${gm[@]}"
    do    
        #echo "array inside lopp:$exclude"
        if [[ " ${exclude[@]} " =~ " $y " ]]; then 
            echo "$y" >> ./$name/excluded/"$x"_excluded_users.json
        else 
            afterExclusion+=( "$y" ) #using for summarazing users in all groups after exclusion
            #echo "$y" >> ./$name/"$x".json  #list of users for checking 
        fi

        #echo "group member:$y"
        #all tieto users in         
        #case "${exclude[@]}" in *"$y"*) echo "$y" >> ./$name/"$x"found_users_by_case.json;; esac

        #create final file with group name and count
        #count how many group memebers "gm" in each group "gf"
        #
        
    done

    # echo $afterExclusion
    #count="$(wc -l ./$name/*.json)" #old soluiton based on created files
    #printf "count;group\n$count" > ./$name/export_test.csv  #old soluiton based on created files

    echo "${#afterExclusion[@]};$x" >> ./$name/length_exclude  #error: need to get count for each group separately
    
    echo "${#gm[@]};$x" >> ./$name/length_gm  #works fine

    #echo "${#afterExclusion[@]}" >> ./$name/length_afterExclusion
done

#todo:
#in how many groups is TietoAllasTutkimus member:#
#https://stackoverflow.com/questions/45099570/bash-mapping-two-arrays/45101204   combining ouptus from two arrays

#loading members of necessary groups: in example "Readers" (all if $filter2 si empty):
mapfile -t allgroups < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$filter2')]"| jq '.[].Name'|tr -d '"') # 
gm_all=()
for z in "${allgroups[@]}"
    do
    echo "mapping members of the group: $z"
    mapfile -t gm_all < <(az ad group member list --group "$z" --query "[].{Name:displayName,objectId:objectId}"| jq '.[].Name'|tr -d '"')
    done

countgroupsinsidegroups=()
for x in "${gf[@]}"
do
    echo "$x"
    for z in "${allgroups[@]}"
    do
        echo "$z"
        for y in "${gm_all[@]}"
        
        do  
            echo "$y"  
            if [[ " $y " =~ " $x " ]]; then
            countgroupsinsidegroups+=( "$x" )
            echo "$x found in: $z " >> "$x"found_in"$z"
            fi
    done
        done

    echo "${#afterExclusion[@]};$x" >> ./$name/length_exclude  #error: need to get count for each group separately
    echo "${#gm[@]};$x" >> ./$name/length_gm  #works fine
    echo "${#countgroupsinsidegroups[@]};$x" >> count_test
done
#echo "${#countgroupsinsidegroups[@]};$z" >> count_test

########################################
#DIRS2=( "powershell" "jq" "azcopy")
#for i in "${DIRS2[@]}"
# do
 #if [ -d "$HOME/apps2/$i" ] ; then

#################### 
###second part if we have some list with groups , better funcion already done in the beggining
# cat ./TietoAllasTutkimus | while read line 
# do
# touch ./$name/"$line".json  # to create file if there are not any members

#     for x in "${line[@]}";
#         do
#             echo "checking and appending if file doesn't exist: $x"        
#             mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') 
#         done
        
#     for y in "${gm[@]}"
#         do   
#             #https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist
#             grep -q -F "$y" ./$name/"$x".json
#             if [ $? -ne 0 ]; then
#             echo "$y" >> ./$name/"$x".json
#             fi
                                
#         done   

# done