#!/bin/bash

#https://github.com/stedolan/jq/issues/370

#https://stackoverflow.com/questions/11426529/reading-output-of-a-command-into-an-array-in-bash

mapfile -t sps < <(az ad sp list --query "[].{Id:to_string(appOwnerTenantId), ObjectId:objectId,Name:displayName}[? ! contains(Name,'billing')&& contains(Id,'d1e1178f-7a71-4d1d-83d7-32ce2952a5a2') && ! contains(Name,'maku') && ! contains(Name,'Billing')|| contains(Id,'null')]" --all| jq '.[].ObjectId'|tr -d '"')

namefilter=$1
gf=0 #group filter
gm=0 #group members
rm -rf ./dataset
mkdir ./dataset

#case 1:
mapfile -t gf < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$namefilter')]"| jq '.[].Name'|tr -d '"')
mapfile -t allgroups < <(az ad group list --query "[].{Name:displayName}" | jq '.[].Name'|tr -d '"')

echo "Start"
for x in "${gf[@]}"
do
    echo "inside for:$x"
  
    mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') #OK and used in mapping
    for y in "${gm[@]}"
    do    
        echo "$y" >> ./dataset/"$x".csv
    done
done

####################
#second part if we have some list with groups 
cat ./TietoAllasTutkimus | while read line 
    do
        touch ./dataset/"$line".csv  # to create file if there are not any members
       
           for x in "${line[@]}";
                do
                    echo "checking and appending if file doesn't exist: $x"        
                    mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') 
                done
            
            for y in "${gm[@]}"
                do   
                    #https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist
                    grep -q -F "$y" ./dataset/"$x".csv
                 if [ $? -ne 0 ]; then
                    echo "$y" >> ./dataset/"$x".csv
                 fi
                                     
                done   
        
    done 
      
#####################