#!/bin/bash

#https://stackoverflow.com/questions/11426529/reading-output-of-a-command-into-an-array-in-bash

#groupmembers=$(az ad group member list -g "HUS Data Prod Published Apotti Extracts Readers" --query [].objectId| tr -d '[]", '| sed '1d;$d') #> ./dataset/_members_test.csv

mapfile -t sps < <(az ad sp list --query "[].{Id:to_string(appOwnerTenantId), ObjectId:objectId,Name:displayName}[? ! contains(Name,'billing')&& contains(Id,'d1e1178f-7a71-4d1d-83d7-32ce2952a5a2') && ! contains(Name,'maku') && ! contains(Name,'Billing')|| contains(Id,'null')]" --all| jq '.[].ObjectId'|tr -d '"')
#mapfile -t groupmembers < <(az ad group member list -g "HUS Data Prod Published Apotti Extracts Readers" --query [].objectId| tr -d '[]", '| sed '1d;$d')
#mapfile -t groups < <(az ad group list --query "[].displayName"| sed '1d;$d'| tr -d ','| sed 's/^..//')
#mapfile -t groupmembers < <(az ad group member list -g "HUS Data Prod Published Apotti Extracts Readers" --query [].objectId| tr -d '[]", '| sed '1d;$d') #> ./dataset/_members_test.csv


#todo>> if name filter is empty then use cat ./TietoAllasTutkimus  
#by using case

#cat json.txt | jq '.Properties .ItmId' | sort | uniq -c | awk -F " " '{print "{\"ItmId\":" $2 ",\"count\":" $1"}"}'| jq .

namefilter=$1
gf=0 #group filter
gm=0 #group members
rm -rf ./dataset
mkdir ./dataset

#case 1:
mapfile -t gf < <(az ad group list --query "[].{Name:displayName}[? contains(Name,'$namefilter')]"| jq '.[].Name'|tr -d '"')
    
    #echo "$smallgroup" > ./dataset/smallgroup.csv
    #mapfile -t gids < <(az ad group list --query "[].{Name:displayName,objectId:objectId}[? contains(Name,'$namefilter')]"| jq '.[].objectId'|tr -d '"')
    #echo "$groupids"

echo "Start"
    #groups=$(az ad group list| jq 'map({"Id": .objectId, "Name": .displayName})')
    #group_size=$(echo $groups| jq length)
for x in "${gf[@]}"
do
    echo "inside for:$x"
        #az ad group member list --group "$x" --query "[].{Name:displayName}" -o tsv > ./dataset/"$x"_members.csv #ok but not used in mapping
        #az ad group member list --group "$x" --query "[].{Name:displayName}"| tr -d '[]", '| sed '1d;$d' > ./dataset/"$x"_list.csv  #NOK {} still there:

        #mapfile groupmembers < <(az ad group member list --group "$x" --query "[].displayName"| tr -d '[]", '| sed '1d;$d') #1  could be used, but output file has bad formatting without spaces
        ##mapfile -t GMMappingObject < <(az ad group member list --group "$x" --query [].objectId| tr -d '[]", '| sed '1d;$d')  #1 object id mapping
    
        #groupmembers_var=$(az ad group member list -g "$x" --query [].displayName| tr -d '[]", '| sed '1d;$d') 
    
    #used:
    mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') #OK and used in mapping
                        #testing:
                           # az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"' > ./dataset/original_wth_excluded_chars.csv
                           # az ad group member list --group "$x" --query "[].{Name:displayName}" > ./dataset/original.csv
    #testing:
        #my_array=()
        #while IFS= read -r line; do
        #   my_array+=( "$line" )
        #done < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"')

        #echo "$GMMappingObject"  > ./dataset/GMMappingObject"$x".csv
        #echo "$groupmembers" > ./dataset/groupmembers_mapfile_"$x".csv
        #echo "$groupmembers_var" > ./dataset/groupmembers_var_"$x".csv
        #echo "$original" > ./dataset/original_mapping_"$x".csv
        #echo "$my_array" > ./dataset/my_array_"$x".csv
    
        #echo "$iteration"
        #echo "${groupids[$iteration]}"

#let iterationX++
#echo "$x"
#echo "$iterationX"
    
    #excluding patern:  #https://stackoverflow.com/questions/3578584/bash-how-to-delete-elements-from-an-array-based-on-a-pattern
       #for index in "${!gm[@]}"
        #do [[ …condition… ]] && unset -v 'gm[$index]' 
        #done
        #gm=("${gm[@]}")
        
    #create list of users in each group:
    for y in "${gm[@]}"
    do       
              
        #grep -q -F "$y" ./dataset/"$x".csv
        #if [ $? -ne 0 ]; then
        echo "$y" >> ./dataset/"$x".csv
        #fi
       
       #echo "$y" >> ./dataset/"$x".csv       
       # if [ "$y" = "$i" ] ; then
       #    echo "Found"
       #    break
       # fi
    done
done

#for i in "${sps[@]}"
#do
 #  echo "$i" >> ./dataset/SPS.csv
  # echo "service principals: $i"
#done
#echo "End"


#####################

#case 2:  https://linuxhint.com/bash_append_array/
#todo because it doesnt work...
#variable outside of the loop
#https://blog.schaal-24.de/skripte/bash-variable-auserhalb-einer-while-schleife/?lang=en

cat ./TietoAllasTutkimus | while read line 
    do
        touch ./dataset/"$line".csv  # to create file if there are not any members
        #echo "$line"
        #gf[ ${#gf[@]} ]="$line"
        #gf=(${gf[@]} "$line")

        #https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist
        #gf+=("$line")                   
        #echo "read line inside loop:$gf"
           for x in "${line[@]}";
                do
                    echo "mapfile x: $x"        
                done
                
        mapfile -t gm < <(az ad group member list --group "$x" --query "[].{Name:displayName}"| jq '.[].Name'|tr -d '"') 
        #echo "$gm" | jq length     

        for y in "${gm[@]}"
        do   
            
            #https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist
            grep -q -F "$y" ./dataset/"$x".csv
            if [ $? -ne 0 ]; then
                echo "$y" >> ./dataset/"$x".csv
            fi
            
             #'count=\${#$y[@]}' 
             #echo "$y;$count" >> ./dataset/count.csv
                #if [ "$y" = "$i" ] ; then
                #echo "$y" >> ./dataset/"$x".csv 
                #break
                #fi
            
                         
        done   
        
    done 
#gf=$gfinside
echo "value outside while:$x"
echo "read line list: $gf"
      
#####################