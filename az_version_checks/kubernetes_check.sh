#!/bin/bash/

outputname=$1

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

az login
az account list
az account show

#todo: swich between accounts 
#az account set --subscription "Eksote Tuotanto 1"
az aks list --output table

#Bash Example:
az aks list --output table > "$outputname"_aks_list_"$month".txt

#Powershell command example: (better parsing Json in here)
#Don't forget to change name and date in the exported file before executing

#pwsh command:
# ((az aks list --query "[].{name: name, resourceGroup: resourceGroup, kubernetesVersion: kubernetesVersion}") | ConvertFrom-Json) |  Export-Csv -path eksote_aks_list_20210218.csv -NoTypeInformation

#Outputs stored in here: https://tietocorporation.sharepoint.com/:f:/r/sites/DDBOperations/Shared%20Documents/00%20-%20Outputs/Kubernetes_versions?csf=1&web=1&e=33Sxhz

#How to use command itslef:
#https://stackoverflow.com/questions/63273456/how-to-export-az-network-public-ip-list-output-to-a-csv
