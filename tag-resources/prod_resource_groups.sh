#!/bin/bash
#az login
#group=$(az group show -n "$line" --query id --output tsv)
#date setup

#todo tag="$1"

mkdir ./logs
mkdir ./wrkdir
mkdir ./outputs

systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

az account show

#policy per resource group
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/tag-policies
#Append a tag and its value from the resource group

#tagging resource groups:

az group list --query "[].{id:id}"| jq '.[].id'|tr -d '"' > ./grouplistIDprodALL.csv
az group list --query "[].{id:id} [? contains(id,'') && ! contains(id,'avaintec') && ! contains(id,'AVAINTEC')]" | jq '.[].id'|tr -d '"' > ./wrkdir/grouplistID_"$time".csv

cat ./wrkdir/grouplistID_"$time".csv | while read line
do
    echo "$line"
        
    az tag update --resource-id "$line" --operation Merge --tags Owner=TietoEvry >> ./logs/logGroup"$time"
    #az tag create --resource-id "$line" --tags Owner=TietoEvry Status=Normal

done

az group list --tag Owner=TietoEvry --output table > ./outputs/resourceGroupsTietoEvry.csv


#policy per resource group
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/tag-policies
#Append a tag and its value from the resource group

#az tag update --resource-id $group --operation Merge --tags CostCenter=00123 Environment=Production

