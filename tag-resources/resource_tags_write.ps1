<#

Connect-AzAccount -Subscription "DDB Data Platform Production"
Connect-AzAccount -Subscription "DDB Data Platform Non Production"
#>

###

#$ImportFile = "C:\temp\Tags\ImportNP_2021_02_04.csv"
$ImportFile = "C:\temp\Tags\ImportPD_2021_02_04.csv"

###

"Reading import file ..."
$CSVData = Import-CSV -Path $ImportFile -Delimiter ";" -ErrorAction Stop
$LinesCount = $CSVData.count
"There are $LinesCount lines in the import file"

"Processing..."
$i = 1
$NrFailed = 0
Foreach ($CSVLine in $CSVData) {
    "$i - $($CSVLine.ResourceID)"

    $AnyUpdate = $false #To mark if any single tag for this object is going to be updated or not
    $UpdatedTags = $null
    $R = $null

    if ( $($CSVLine.Businessunit) -ne "") {
        $UpdatedTags += @{"businessUnit" = $($CSVLine.Businessunit)}
        $AnyUpdate = $true
    }
    if ( $($CSVLine.License) -ne "") {
        $UpdatedTags += @{"License" = $($CSVLine.License)}
        $AnyUpdate = $true
    }
    if ( $($CSVLine.ManagedBy) -ne "") {
        $UpdatedTags += @{"ManagedBy" = $($CSVLine.ManagedBy)}
        $AnyUpdate = $true
    }
    if ( $($CSVLine.Environment) -ne "") {
        $UpdatedTags += @{"Environment" = $($CSVLine.Environment)}
        $AnyUpdate = $true
    }
    if ( $($CSVLine.Region) -ne "") {
        $UpdatedTags += @{"Region" = $($CSVLine.Region)}
        $AnyUpdate = $true
    }
    if ( $($CSVLine.ApplicationID) -ne "") {
        $UpdatedTags += @{"ApplicationID" = $($CSVLine.ApplicationID)}
        $AnyUpdate = $true
    }

    if ( $AnyUpdate ) {   
        "Tags to be set:"
        $UpdatedTags
        try {
            Update-AzTag -ResourceId $CSVLine.ResourceID -Tag $UpdatedTags -Operation Replace
        }
        catch {
            "UPDATE FAILED!!!"
            $NrFailed++
        }
    } else {
        "No update for this resource (group)"
    }     


    $i++
}

"Total processed: $($i-1), Total failed: $NrFailed"

