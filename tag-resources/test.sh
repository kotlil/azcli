#!/bin/bash
#az login
#group=$(az group show -n "$line" --query id --output tsv)
az account show

az account list --output table
az account list --output json --query "[].{name:name}"| jq '.[].name'|tr -d '"' > ./subscriptions.csv

cat ./subscriptions.csv

#set suscription
#az account set --subscription "Eksote Testi 1"

#az resource list --output json --query "[].{name:name}"| jq '.[].name'|tr -d '"' > ./resourcelist.csv
#az resource list --output table 

az resource list --query "[].{id:id}"| jq '.[].id'|tr -d '"' > ./resourcelistID.csv

#az resource show -g cloud-shell-storage-northeurope -n csa100320009af8bc17 --reource-type Microsoft.Storage/storageAccounts > IDtest.csv

cat ./resourcelistID.csv | while read line
do
    echo "$line"
        
    az tag update --resource-id "$line" --operation Merge --tags Owner=TietoEvry >> ./log 
    #az tag create --resource-id "$line" --tags Owner=TietoEvry Status=Normal

done
