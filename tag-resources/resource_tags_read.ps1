# Connect-AzAccount
# Select-AzSubscription "DDB Data Platform Non Production"
# Select-AzSubscription "DDB Data Platform Production"

$FileName = "c:\temp\tags\tagsPD_21_02_04_B.csv"
$OutputTags = @()

# Process resources
$AllResources = Get-AzResource
Foreach ($Resource in $AllResources) {

    <#
    # Skip unwanted resources from listing
    if ( $Resource.ResourceType -eq "Microsoft.Sql/managedInstances/databases" ) { continue }
    if ( $Resource.ResourceType -eq "Microsoft.Compute/restorePointCollections" ) { continue }
    if ( $Resource.ResourceType -eq "Microsoft.Portal/dashboards" ) { continue }
    if ( $Resource.ResourceType -eq "Microsoft.Compute/virtualMachines/extensions" ) { continue }
    if ( $Resource.ResourceType -eq "Microsoft.Automation/automationAccounts/runbooks" ) { continue }
    if ( $Resource.ResourceType -like "Microsoft.Insights/*" ) { continue }
    #>
    
    # Reset all specific tags variables to empty string
    $AllTagsString = ""
    $Tag_businessUnit = ""
    $Tag_license = ""
    $Tag_ManagedBy = ""
    $Tag_Environment = ""
    $Tag_Region = ""
    $Tag_ApplicationID = ""

    Foreach ($TagEntry in $($Resource.Tags.Keys)) {
        $AllTagsString += "$TagEntry : $($Resource.Tags.Item($TagEntry)),  "
        
        if ($TagEntry -eq "businessUnit") { $Tag_businessUnit = $($Resource.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "license") { $Tag_license = $($Resource.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "ManagedBy") { $Tag_ManagedBy = $($Resource.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "Environment") { $Tag_Environment = $($Resource.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "Region") { $Tag_Region = $($Resource.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "ApplicationID") { $Tag_ApplicationID = $($Resource.Tags.Item($TagEntry)) }
    }

    $OutputTags += [pscustomobject] @{
        ResourceID = $($Resource.ResourceId)
        ResourceGroup = $($Resource.ResourceGroupName)
        ResourceName = $($Resource.Name)
        ResourceType = $($Resource.Type)
        BusinessUnit = $Tag_businessUnit
        License = $Tag_license
        ManagedBy = $Tag_ManagedBy
        Environment = $Tag_Environment
        Region = $Tag_Region
        ApplicationID = $Tag_ApplicationID
        AllTags = $AllTagsString
    }

}


# Process resource groups
$AllResourceGroups = Get-AzResourceGroup
Foreach ($ResourceGroup in $AllResourceGroups) {

    # Reset all specific tags variables to empty string
    $AllTagsString = ""
    $Tag_businessUnit = ""
    $Tag_license = ""
    $Tag_ManagedBy = ""
    $Tag_Environment = ""
    $Tag_Region = ""
    $Tag_ApplicationID = ""

    Foreach ($TagEntry in $($ResourceGroup.Tags.Keys)) {
        $AllTagsString += "$TagEntry : $($ResourceGroup.Tags.Item($TagEntry)),  "

        if ($TagEntry -eq "businessUnit") { $Tag_businessUnit = $($ResourceGroup.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "license") { $Tag_license = $($ResourceGroup.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "ManagedBy") { $Tag_ManagedBy = $($ResourceGroup.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "Environment") { $Tag_Environment = $($ResourceGroup.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "Region") { $Tag_Region = $($ResourceGroup.Tags.Item($TagEntry)) }
        if ($TagEntry -eq "ApplicationID") { $Tag_ApplicationID = $($ResourceGroup.Tags.Item($TagEntry)) }
    }

    $OutputTags += [pscustomobject] @{
        ResourceID = $($ResourceGroup.ResourceId)
        ResourceGroup = $($ResourceGroup.ResourceGroupName)
        ResourceName = $($ResourceGroup.ResourceGroupName)
        ResourceType = "Resource group"
        BusinessUnit = $Tag_businessUnit
        License = $Tag_license
        ManagedBy = $Tag_ManagedBy
        Environment = $Tag_Environment
        Region = $Tag_Region
        ApplicationID = $Tag_ApplicationID
        AllTags = $AllTagsString
    }

}

$OutputTags | Export-CSV -Path $FileName -NoTypeInformation -Encoding UTF8