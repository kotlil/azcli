#!/bin/bash
#az login
#group=$(az group show -n "$line" --query id --output tsv)
#date setup
mkdir ./logs
mkdir ./wrkdir
mkdir ./outputs


systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

az account show

#az account list --output table
#az account list --output json --query "[].{name:name}"| jq '.[].name'|tr -d '"' > ./subscriptions.csv
#cat ./subscriptions.csv
#set suscription:
#az account set --subscription "Eksote Testi 1"

#az resource list --output json --query "[].{name:name}"| jq '.[].name'|tr -d '"' > ./resourcelist.csv
#az resource list --output table 

az resource list --query "[].{id:id}"| jq '.[].id'|tr -d '"' > ./resourcelistIDprodALL.csv
#doesn't contains avaintec
az resource list --query "[].{id:id} [? contains(id,'') && ! contains(id,'avaintec') && ! contains(id,'AVAINTEC')]" | jq '.[].id'|tr -d '"' > ./wrkdir/resourcelistID_"$time".csv

#az resource show -g cloud-shell-storage-northeurope -n csa100320009af8bc17 --reource-type Microsoft.Storage/storageAccounts > IDtest.csv

cat ./wrkdir/resourcelistID_"$time".csv | while read line
do
    echo "$line"
        
    az tag update --resource-id "$line" --operation Merge --tags Owner=TietoEvry >> ./logs/log"$time"
    #az tag create --resource-id "$line" --tags Owner=TietoEvry Status=Normal

done

az resource list --tag Owner=TietoEvry --output table > ./outputs/resourcesTietoEvry.csv