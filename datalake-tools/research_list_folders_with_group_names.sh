#!/bin/bash
#az login
rm -rf ./output
mkdir ./output
account="husdltu1adls"
rootpath="/clusters/maindatalake/storage"

groups=$(az ad group list --output json)  #https://docs.microsoft.com/en-us/cli/azure/ad/group?view=azure-cli-latest#az_ad_group_list
echo $groups > groups.json

#https://docs.microsoft.com/en-us/azure/data-lake-store/data-lake-store-get-started-cli-2.0
#https://docs.microsoft.com/en-us/cli/azure/dls?view=azure-cli-latest

#possible alternative to: Get-AzureRmDataLakeStoreChildItem -AccountName $account -Path $rootpath | %{$_.Name}

folders=$(az dls fs list --account "$account" --path "$rootpath") # todo: pathSuffix save to var with using jq  | pathSuffix": "mustiradu" >
echo $folders > folders.json

mapfile -t pathSuffix< <(az dls fs list --account "$account" --path "$rootpath"|jq '.[].pathSuffix'|tr -d '"') # todo: pathSuffix save to var with using jq  | pathSuffix": "mustiradu" >
echo $pathSuffix > pathSuffix.json

for x in "${pathSuffix[@]}"
do
#echo "$x"

printf "\n$x;" >> ./output/final_output.csv
   #alterniative 
   #Get-AzureRmDataLakeStoreItemAclEntry -AccountName husdltu1adls -Path "/clusters/maindatalake/storage/apotti_extracts" 

#final query creates files:
#   az dls fs access show --account "$account" --path "$rootpath/$x" --output table --query "entries[? contains(@,'default:group')] | [1:6] " | grep -Eo -i '[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}' > ./output/"$x"_grep.json     

#storing same query into variable:

      mapfile -t acl< <(az dls fs access show --account "$account" --path "$rootpath/$x" --output table --query "entries[? contains(@,'default:group')] | [1:6] " | grep -Eo -i '[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}');  #https://jmespath.org/tutorial.html
        
        for y in "${acl[@]}"
        do
            printf "$y;" >> ./output/test0.log
            
            #final query creates files:
            #az ad group show --query "displayName" -g "$y" >> ./output/"$x"_displayName.csv
            mapfile -t displnm< <(az ad group show --query "displayName" -g "$y") 
            printf "$displnm;" >> ./output/final_output.csv
            echo "$displnm;" >> ./output/test1.log

        done
   echo "$x;$acl" >> ./output/test2.log 
done
   #testing queries:
      #mapfile -t acl< <(az dls fs access show --account "$account" --path "$rootpath/$x" --query "entries[? contains(@, 'default:group')] | [1:6]");  #https://jmespath.org/tutorial.html         

         #az dls fs access show --account "$account" --path "$rootpath/$x" > ./output/"$x".json #az dls fs access show --account "husdltu1adls" --path "/clusters/maindatalake/storage/mustiradu
         #az dls fs access show --account "$account" --path "$rootpath/$x" --query "entries[? contains(@,'default:group')] | [1:6]" > ./output/"$x"_jmespath.json
         #az dls fs access show --account "$account" --path "$rootpath/$x" --output table --query "entries[? contains(@,'default:group')] | [1:6] " > ./output/"$x"_table.json
         #az dls fs access show --account "$account" --path "$rootpath/$x" --output table --query "entries[? contains(@,'default:group')] | [1:6] " | sed s/"default:group:"// > ./output/"$x"_sed.json
         #az dls fs access show --account "$account" --path "$rootpath/$x" --output tsv > ./output/"$x"_tsv.json
         #az dls fs access show --account "$account" --path "$rootpath/$x" --query "entries[*] | [1:6] |  " > ./output/"$x"_tagsjmespath.json

      #testing queries / raw json data:
      #   az dls fs access show --account "$account" --path "$rootpath/$x" --output json > ./output/"$x"_raw.json     
      #   az dls fs access show --account husdltu1adls --path "/clusters/maindatalake/storage/bcb_astma" --output json > ./output/bcb_astma_raw.json


         #https://regex101.com/r/6pA9Rk/64
         #https://stackoverflow.com/questions/42047994/regex-how-to-find-a-guid-in-a-long-string
         #https://stackoverflow.com/questions/7905929/how-to-test-valid-uuid-guid/13653180#13653180
         #https://stackoverflow.com/questions/7129279/sed-how-to-remove-everything-but-a-defined-pattern
         #https://www.unix.com/shell-programming-and-scripting/181533-replace-everything-but-pattern-line-using-sed.html

  
   #echo $acl >> "$rootpath/$x".json
   # for y in "${acl[@]}"
   # do 
   # done
   
   #  \b[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}\b
   #https://regex101.com/
   #http://regex.inginf.units.it/

   #https://docs.microsoft.com/en-us/cli/azure/dls/fs/access?view=azure-cli-latest
   #az ad group show --query "displayName" -g 0d275816-7a1d-4847-884d-b5c67f473973 -o tsv

   #example:
   #apotti_extracts_jmespath
   
         #az ad group show --query "displayName" -g 0d275816-7a1d-4847-884d-b5c67f473973 > ./output/test.txt
         #az ad group show --query "displayName" -g 106004df-6d49-450e-a180-e5bfa1fef9b3 >> ./output/test.txt

   #my question:
   #https://stackoverflow.com/questions/65999976/how-to-parse-multidimensional-json-object-colon-separated-into-array-need-to-se

   #https://stackoverflow.com/questions/42047994/regex-how-to-find-a-guid-in-a-long-string
   #https://regex101.com/r/6pA9Rk/1

   
   #https://stackoverflow.com/questions/11233825/multi-dimensional-arrays-in-bash


   #https://docs.microsoft.com/en-us/cli/azure/query-azure-cli

   #https://unix.stackexchange.com/questions/443884/match-keys-with-regex-in-jq

   #https://unix.stackexchange.com/questions/481288/get-values-for-a-given-key-and-its-parent-with-jq


# az dls fs access show --account mydatalakestoragegen1 --path /mynewfolder/vehicle1_09142014.csv

#possible alternative to: Get-AzureRmDataLakeStoreItemAclEntry -AccountName $account -Path $($rootpath + "/" + $_)
#Get-AzureRmDataLakeStoreItemAclEntry -AccountName husdltu1adls -Path /clusters/maindatalake/storage/mustiradu

#acl=$(az dls fs access show --account "$account" --path "/clusters/maindatalake/storage/mustiradu")  #todo: show diplayname
#echo $acl > acl.json

