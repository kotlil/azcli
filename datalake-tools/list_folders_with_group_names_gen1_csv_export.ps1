# This script lists folders in the rootpath with the AD groups and related AD group names.
# NOTE: Run this scirpt in the integrated shell in the Azure Portal for maximum performance in recurisve file operations

$account="husdltu1adls"
$rootpath="/clusters/maindatalake/storage"

$outfile = "adls_pwsh.csv"
$finallfile = "adls_pwsh_finall.csv"

$groups = Get-AzADGroup
$folders = Get-AzureRmDataLakeStoreChildItem -AccountName $account -Path $rootpath | %{$_.Name}
$folders |
    %{ [tuple]::Create($_, $(Get-AzureRmDataLakeStoreItemAclEntry -AccountName $account -Path $($rootpath + "/" + $_))) } |
    %{ [tuple]::Create([string]$_.item1,$($_.item2 | %{ $gr = $_.Id; $groups | %{ if ($gr -eq $_.Id) { $_.DisplayName } } }) -join ",") } | Export-CSV -Path $outfile -Force;

    #array operations:
    #edit 1: array to string for item1# %{ [tuple]::Create([string]$_.item1,$($_.item2  # name in output were shortend  
    #edit 2: -join "," for item2  #https://stackoverflow.com/questions/23417896/powershell-create-a-comma-delimited-list-from-array-of-ips-that-occur-n-number

#removing '"' from output csv file
Get-Content $outfile | % {$_ -replace '"',''} | Out-File -Path $finallfile -Force