#!/bin/bash
#az login

#cleaning
rm -rf ./output
mkdir ./output

#date setup
systemtime=`date +"%Y %m %d %H:%M"`
time=$(echo $systemtime | tr -d ' ' | tr -d ':')  #removed spaces amd ":" from systemtime
month=`date +"%Y-%m"`
echo "date and time:$time"

#OneDrive setup
OneDrive='/mnt/c/Users/kotunpet/OneDrive - TietoEVRY'  #change path if you want automatically save and share all output files
mkdir -p "$OneDrive"/ADLS_permissions/$month/
echo "OneDrive path: $OneDrive/ADLS_permissions/$month/"

#account setup
account="husdltu1adls"
rootpath="/clusters/maindatalake/storage"


groups=$(az ad group list --output json)
echo $groups > groups.json

folders=$(az dls fs list --account "$account" --path "$rootpath") 
echo $folders > folders.json


mapfile -t pathSuffix< <(az dls fs list --account "$account" --path "$rootpath"|jq '.[].pathSuffix'|tr -d '"')
echo $pathSuffix > pathSuffix.json

for x in "${pathSuffix[@]}"
do

printf "\n$x;" >> ./output/finall_output.csv
     mapfile -t acl< <(az dls fs access show --account "$account" --path "$rootpath/$x" --output table --query "entries[? contains(@,'default:group')] | [1:6] " | grep -Eo -i '[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}'); 
        
        for y in "${acl[@]}"
        do
            printf "$y;" >> ./output/test0.log
         
            mapfile -t displnm< <(az ad group show --query "displayName" -g "$y") 
            printf "$displnm;" >> ./output/finall_output.csv
            echo "$displnm;" >> ./output/test1.log

        done
   echo "$x;$acl" >> ./output/test2.log 
done

#copy to OneDrive
cp ./output/finall_output.csv "$OneDrive"/ADLS_permissions/$month/ADLS_permissions_"$time".csv