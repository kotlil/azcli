dbutils.widgets.text("integration","lifecare", "1. Integration")
dbutils.notebook.run("map-disks",60)

val integrationName : String = dbutils.widgets.get("integration")

val stagingPath = s"dbfs:/mnt/staging/eksote/${integrationName}/"
val storagePath = s"dbfs:/mnt/storage/eksote/${integrationName}/"

val stor = dbutils.fs.ls(storagePath).map(_.path.replace(storagePath,"").toString.replace("/",""))
val stag = dbutils.fs.ls(stagingPath).map(_.path.replace(stagingPath,"").toString)

import scala.collection.mutable.ListBuffer
var errors = new ListBuffer[String]()
// make sure that there is same amount of files in stagin and storage
val df = stor diff stag
if (df.size != 0) {
  throw new Exception(s"Different number of files in staging:${stag.count} and storage:${stor.count}")
}

stag.foreach(file => {
  val stagDF = spark.read.parquet(stagingPath + file)
  val storDF = spark.read.parquet(storagePath + file)
  if (stagDF.count != storDF.count) {
    errors += s"Different row count in staging and storage: ${file} "
    print(" --- different number of rows ---")
  }
  println
})

if ( errors.size > 0) {
  errors.foreach(println)
  throw new Exception("Failed: " + errors.mkString(","))
} else {
  println("OK")
}