//kotula's modifications
val integration : String = dbutils.widgets.get("integration")
val setInstrument : String = dbutils.widgets.get("instrument")
val dates = dbutils.fs.ls(s"/mnt/storage/kymsote/${integration}/${setInstrument}").map(_.name.replace("/",""))
//val dates = dbutils.fs.ls("/mnt/storage/kymsote/raisoft/23").map(_.name.replace("/",""))
//println(dates)

val instruments = List(23,24,62,125,144)

//csv formatting
println("Instrument,Date,Assessments,Results,Questions,Exception")


def checkRowCounts(instrument: Int, date: String) { 
 
  try {
  
    val df = spark.read.parquet(s"/mnt/storage/kymsote/raisoft/${instrument}/${date}/assessments")
    val df2 = spark.read.parquet(s"/mnt/storage/kymsote/raisoft/${instrument}/${date}/result")
    val df3 = spark.read.parquet(s"/mnt/storage/kymsote/raisoft/${instrument}/${date}/questions")
    
    println(s"${instrument},${date},${df.count},${df2.count},${df3.count}")
    
    //println(s"/mnt/storage/kymsote/raisoft/${instrument}/${date}")
    //println("Assessments: " + df.count)
    //println("Results:     " + df2.count)
    //println("Questions:   " + df3.count)  

  }catch {
        
    case e: Exception => println(s"${instrument},${date},x,x,x,Failed to read assestments from /mnt/storage/kymsote/raisoft/${instrument}/${date}")
    //println(s"Failed to read assestments from /mnt/storage/kymsote/raisoft/${instrument}/${date}")
  }
}

dates.foreach(date => {
  instruments.foreach(ins => {
    checkRowCounts(ins,date)
  })
})

