%python
import os, time, datetime

#dbutils.notebook.run("mount_social_vol",60)
dbutils.notebook.run("mount_health_vol",60)

dbutils.widgets.text("integration","","1. Integration")

integrationName = dbutils.widgets.get("integration")

path = '/dbfs/mnt/staging/keusote/backup/' + integrationName
print(path)

#checking if is folder empty
def recur(item):
  good_to_delete_me = True
 # print(item + 'inside')
  item = item.replace('/dbfs','dbfs:')
  contents = dbutils.fs.ls(item)
  # contents = dbutils.fs.ls(item)
  # print(contents)
  for i in contents:
    if not i.isDir():
      good_to_delete_me = False
    else:
      can_delete_child = recur(i.path)
      good_to_delete_me = good_to_delete_me and can_delete_child
      if can_delete_child:
        test= i.path
        # print(test)
        # dbutils.fs.rm(test)
  return good_to_delete_me

#
def remove_folder(item):
  item = item.replace('/dbfs','dbfs:')
  dbutils.fs.rm(item)

#loop folders in storage
for root, dirs, files in os.walk(path, topdown=False):
    for name in dirs:
      item = (os.path.join(path, name))
      if recur(item):
        print('Good to delete:' + item)
        remove_folder(item)  #calling remove fodler function
