%python
import os, time, datetime

#dbutils.notebook.run("mount_social_vol",60)
dbutils.notebook.run("mount_health_vol",60)
dbutils.widgets.text("integration","","1. Integration")

integrationName = dbutils.widgets.get("integration")

print(integrationName)

compare_time  = datetime.datetime.now() - datetime.timedelta(hours = 22)

failed =[]
path = '/dbfs/mnt/staging/keusote/' + integrationName

print(path)

for subdir, dirs, files in os.walk(path):
  for filename in files:
    filepath = subdir + os.sep + filename
    mod_time = os.path.getmtime(filepath) #time.ctime(os.path.getmtime(filepath))
    if (datetime.datetime.utcfromtimestamp(mod_time) < compare_time):
        failed.append(filename)
if(len(failed) > 0):
   raise Exception(failed)
