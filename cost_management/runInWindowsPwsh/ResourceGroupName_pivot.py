import os
import sys
import win32com.client as win32
from pywintypes import com_error
from pathlib import Path
import pandas as pd  # only used for synthetic data
import numpy as np  # only used for synthetic data
import random  # only used for synthetic data
from datetime import datetime  # only used for synthetic data

#interface
import tkinter as tk
from tkinter import filedialog
import pandas as pd


win32c = win32.constants

def main():
    global f_path
    global path
    # sheet name for data
    sheet_name = 'Data'  # update with sheet name from your file
    
    # file path
    path = filedialog.askopenfilename()
    print(path)

    f_path = os.path.split(path)[0] + "/"

    f_name = os.path.split(path)[1]
    
    print(f_path)
    print(f_name)
    
    run_excel(f_path, f_name, sheet_name)

########
def run_excel(f_path: Path, f_name: str, sheet_name: str):

    filename = f_path + f_name
    # Workbooks = ()
    # create excel object
    excel = win32.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True  # False
    
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename) #, ReadOnly = False
    except com_error as e:
        if e.excepinfo[5] == -2146827284:
            print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        else:
            raise e
        sys.exit(1)

    # set worksheet
    ws1 = wb.Sheets('Data')
    
    # Setup and call pivot_table
    ws2_name = 'pivot_table'
    wb.Sheets.Add().Name = ws2_name
    ws2 = wb.Sheets(ws2_name)
    
    pt_name = 'pivot'
    
    pt_rows = ['ResourceGroupName']
    pt_cols = ['UsageDate']
    pt_filters = ['UsageDate']
    
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    
    #NUMBER FORMATS
    #https://xlsxwriter.readthedocs.io/format.html#set_num_format

    pt_fields = [['Cost', '% of row total', win32c.xlSum, ''], #format needs to be changed manually in UI to: show valuse as % of row total
                ['Cost', 'Sum of Cost', win32c.xlSum, '$#,##0.00'],
                ['Cost', 'Cost: count', win32c.xlCount, '0']] 
    
    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields)
    
#     wb.Close(True)
#     excel.Quit()


#######
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 2
    
    # grab the pivot table source data
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=ws1.UsedRange)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C1', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()

    # Sets the rows, columns and filters of the pivot table

    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1

    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]

    # Visiblity True or Valse
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True

main()