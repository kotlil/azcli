#!/bin/bash

#https://docs.microsoft.com/en-us/cli/azure/costmanagement?view=azure-cli-latest

#az login

periodfrom="2018-06-01T00:00:00Z" 
periodto="2018-10-31T00:00:00Z"

subscription="27b2a9c9-89f6-44cf-b4ec-74df29dce1fd"
echo $subscription

az costmanagement query --type "Usage" --timeframe "TheLastMonth" --scope "subscriptions/$subscription"
#az costmanagement query --type "Usage" --timeframe "TheLastMonth" --scope "subscriptions/$subscription" --dataset-filter "{\"and\":[{\"or\":[{\"dimension\":{\"name\":\"ResourceLocation\",\"operator\":\"In\",\"values\":[\"East US\",\"West Europe\"]}},{\"tag\":{\"name\":\"Environment\",\"operator\":\"In\",\"values\":[\"UAT\",\"Prod\"]}}]},{\"dimension\":{\"name\":\"ResourceGroup\",\"operator\":\"In\",\"values\":[\"API\"]}}]}"
#az costmanagement export create --name "TestExport" --type "Usage" --dataset-configuration columns="Date" columns="MeterId" columns="InstanceId" columns="ResourceLocation" columns="PreTaxCost" --dataset-grouping name="SubscriptionName" type="Dimension" --dataset-grouping name="Environment" type="Tag" --timeframe "MonthToDate" --storage-container="exports" --storage-account-id="/subscriptions/$subscription/resourceGroups/MYDEVTESTRG/providers/Microsoft.Storage/storageAccounts/ccmeastusdiag182" --storage-directory="ad-hoc" --recurrence "Weekly" --recurrence-period from="2018-06-01T00:00:00Z" to="2018-10-31T00:00:00Z" --schedule-status "Active" --scope "subscriptions/$subscription"
#az costmanagement export show --name "TestExport" --scope "subscriptions/$subscription" #--timeframe "TheLastMonth" > 


#--timeframe
#accepted values: BillingMonthToDate, Custom, MonthToDate, TheLastBillingMonth, TheLastMonth, WeekToDate

#--type
#accepted values: ActualCost, AmortizedCost, Usage