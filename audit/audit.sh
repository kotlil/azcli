#!/bin/bash
#https://azurecitadel.com/prereqs/cli/cli-4-bash/
#https://docs.microsoft.com/cs-cz/cli/azure/use-cli-effectively

mkdir ./examples
rm -rf ./examples/err.log
rm -rf ./examples/final_output_raw.json
#az login

#used for testing loop:     && ! contains(resourceType,'microsoft.security')
#az resource list --output json --query "[].{id:id, resourceGroup:resourceGroup, type:type}[? contains(id,'husdl-tu1-doccano')&& ! contains(type,'microsoft.compute/restorepointcollections')&& ! contains(type,'microsoft.compute/disks')&& ! contains(type,'microsoft.compute/restorepointcollections')]"| jq '.[].id'|tr -d '"' > ./examples/needed_resources_doccano.csv
az resource list --output json --query "[].{id:id, resourceGroup:resourceGroup, type:type}[? contains(id,'hus') && ! contains(id,'restorePointCollections') && ! contains(id, 'disks') && ! contains(id,'restorepointcollections')&& ! contains(id,'vm') && ! contains(id,'microsoft.compute/availabilitysets') && ! contains(type,'microsoft.compute/availabilitysets') && ! contains(id,'microsoft.security.') && ! contains(type,'microsoft.security') && ! contains (id,'microsoft.web/connections') && ! contains (type,'microsoft.web/connections') && ! contains (type,'microsoft.compute/virtualmachines/extensions') && ! contains (type,'microsoft.insights/actiongroups') && ! contains (type,'microsoft.insights/metricalerts') && ! contains (type,'microsoft.alertsmanagement') && ! contains (type,'microsoft.operationsmanagement')]"| jq '.[].id'|tr -d '"' > ./examples/needed_resources_hus.csv
###

#
#cat ./examples/needed_resources_doccano.csv | while read line
cat ./examples/needed_resources_hus.csv | while read line
do
echo "$line"
printf "$line\n" >> ./examples/err.log

az monitor diagnostic-settings list --resource "$line" &>> ./examples/final_output_raw.json 2>>./examples/err.log  #>file.log 2>&1 

#az monitor diagnostic-settings list --resource "$line" --query "value[?resourceGroup == 'husdl-tu1-doccano'].name" >> ./examples/final_output_doccano_query.json
done
cat ./examples/err.log | grep -B 1 "NotSupported" > ./examples/NotSupported.txt
cat ./examples/err.log | grep -B 1 "was not found" > ./examples/WasNotFound.txt  
