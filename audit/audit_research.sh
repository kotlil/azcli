#!/bin/bash
#https://azurecitadel.com/prereqs/cli/cli-4-bash/
#https://docs.microsoft.com/cs-cz/cli/azure/use-cli-effectively

mkdir ./examples
#az login
#az account list > accounts.json

#az resource list --output json --query "[].{name:name, resourceGroup:resourceGroup, type:type, sku:sku.name, kind:kind, location:location }" > output.json

az resource list --output json --query "[].{id:id}" > all_resources_id.json
az resource list --output tsv --query "[].{id:id}" > all_resources_id.csv
#used for testing loop:
az resource list --output json --query "[].{id:id, resourceGroup:resourceGroup}[? contains(id,'husdl-tu1-doccano')]"| jq '.[].id'|tr -d '"' > ./examples/resources_contains_resourceGroup_husdl-tu1-doccano.csv
###
az resource list --output json --query "[].{name:name}" > all_resources_name.json
az resource list --output tsv --query "[].{name:name}" > all_resources_name.csv

az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}" > all_resources_needed_parameters.json
az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}[? contains(resourceGroup,'husdl-tu1') && ! contains(id,'Network') && ! contains(id,'vm') && ! contains(type,'disks') && ! contains(type,'connections')]" > all_resources_excluded_parameters.json

#az resource list --output json --query "[].{id:id, name:name}[? contains(id,'doccano')]"| jq '.[].id' > output.json


#az ad sp list --query "[].{Id:to_string(appOwnerTenantId), ObjectId:objectId,Name:displayName}[? ! contains(Name,'billing')&& contains(Id,'d1e1178f-7a71-4d1d-83d7-32ce2952a5a2') && ! contains(Name,'maku') && ! contains(Name,'Billing')|| contains(Id,'null')]" --all| jq '.[].ObjectId'|tr -d '"'
az resource list --output json --query "[].{id:id, name:name}[? contains(id,'databricks')&& contains(id,'servers') && ! contains(id,'vm')]" > ./examples/query_combination.json

az resource list --output json --query "[].{id:id, name:name}[? contains(id,'doccano')]" > ./examples/contains_doccano.json


az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}[? contains(resourceGroup,'husdl-tu1') && contains(id,'vaults') && ! contains(id,'Network') && ! contains(id,'vm') && ! contains(type,'disks') && ! contains(type,'connections')]"| jq '.[].id' > ./examples/contains_vaults.json
#az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}[? contains(resourceGroup,'husdl-tu1') && contains(id,'vaults') && ! contains(id,'Network') && ! contains(id,'vm') && ! contains(type,'disks') && ! contains(type,'connections')]"| jq '.[].id' > ./examples/contains_vaults.csv
#az resource list --output json --query "[].{id:id, name:name}[? contains(id,'vaults')]" > ./examples/contains_vaults.json
az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}[? contains(resourceGroup,'husdl-tu1') && contains(id,'databricks') && ! contains(id,'Network') && ! contains(id,'vm') && ! contains(type,'disks') && ! contains(type,'connections')]" > ./examples/contains_databricks.json
az resource list --output json --query "[].{name:name, type:type, resourceGroup:resourceGroup, id:id}[? contains(resourceGroup,'husdl-tu1') && contains(id,'sql') && ! contains(id,'Network') && ! contains(id,'vm') && ! contains(type,'disks') && ! contains(type,'connections')]" > ./examples/contains_sql.json 
#
az resource list --output json --query "[].{id:id, name:name}[? contains(id,'LogicAppsManagement')]" > ./examples/contains_LogicAppsManagement.json


#example
#az monitor diagnostic-settings categories list -g myRG --resource-type microsoft.logic/workflows --resource myWorkflow

#resourceGroup = $(az monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv")
#echo "resourceGroup is:$resourceGroup"

#jmespath
az monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv" > ./examples/doccano_example_no_query.json
az monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv" --query "value[?resourceGroup == 'husdl-tu1-doccano'].name" > ./examples/doccano_example_query_resourceGroup.json
az monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv" --query "value[?category == 'AuditEvent'].logs" > ./examples/doccano_example_query_audit.json


#another type of command, specified separately
az monitor diagnostic-settings list --subscription 27b2a9c9-89f6-44cf-b4ec-74df29dce1fd --resource "doccano-prod-kv" --resource-group "husdl-tu1-doccano" --resource-type "Microsoft.KeyVault/vaults"
#az monitor diagnostic-settings list --subscription 27b2a9c9-89f6-44cf-b4ec-74df29dce1fd --resource doccano-prod-kv --resource-type "Microsoft.KeyVault/vaults"
#az monitor diagnostic-settings list --subscription 27b2a9c9-89f6-44cf-b4ec-74df29dce1fd --resource doccano-prod-kv --resource-group husdl-tu1-doccano --resource-type Microsoft.KeyVault

#todo: for specific groups
cat ./examples/resources_contains_resourceGroup_husdl-tu1-doccano.csv | while read line
do
az monitor diagnostic-settings list --resource "$line" >> ./examples/final_output_doccano.json
az monitor diagnostic-settings list --resource "$line" --query "value[?resourceGroup == 'husdl-tu1-doccano'].name" >> ./examples/final_output_doccano_query.json
done
#value=$(jq -r '.id' output.json)
#echo "$value"



#cat output.tsv | while read line
#do

#az monitor diagnostic-settings list --resource "doccano-prod-kv-audit-log-setting"
#name=$(az monitor diagnostic-settings list --resource "$line" --query [].name)
#az monitor diagnostic-settings list --resource "$line" --query "value[?resourceGroup == husdl-tu1-doccano ].{id:id, name:name}]" > $name.json

#diagsettings = $(az monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv")
#echo "$name and $line"

#az monitor diagnostic-settings list --resource "$line"
#printf "$line;$diagsettings" >> ./"$name"outputs.json
#done