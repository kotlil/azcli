#https://linuxconfig.org/how-to-parse-data-from-json-into-python
#https://pypi.org/project/az.cli/#description

import json
from az.cli import az
import os, sys

#diagnostic_output = open("./diagnostic.json", "w")
#login = az('login')

# parsed_json = (json.loads(json_data))
# print(json.dumps(parsed_json, indent=4, sort_keys=True))

# loaded_json = json.loads(json_data)
# for x in loaded_json:
# 	print("%s: %d" % (x, loaded_json[x]))

#     class Test(object):
#     def __init__(self, data):
# 	    self.__dict__ = json.loads(data)

# test1 = Test(json_data)
# print(test1.a)

exit_code, command_result2, logs = az('ad group list --query "[].{Name:displayName, objectId:objectId}"')

# On 0 (SUCCESS) print result_dict, otherwise get info from `logs`
if exit_code == 0:
    print (command_result2)
    #sys.stdout.close ("json.json", "w")
    
else:
    print (logs)
###

#exit_code, members1, logs = az('ad group member list --group "HUS Data Prod Staging Clinisoft Writers" --query "[].{Name:displayName}"')
exit_code, members1, logs = az('resource list --output json --query "[].{id:id, name:name}[? contains(id,`doccano`)]"')

if exit_code == 0:
    data_dict = json.dumps(members1) #json conversion from single quotes output to double quotes formating
    print (data_dict)
    json_object = json.loads(data_dict) 

    for m in json_object:
        idm = (m['id'])
        #print(m['id'])
        print(idm)
        namem = (m['name'])  
        print(namem)
        diagnostic_output = open("%s.json" % namem, "w") 
        #diagnostic_output = open("test.json", "w")
                          # showing only name from double quotes "virtual json" which is not created physically, values are picked from command result...
                          #raiese error:  TypeError: string indices must be integers
                          #how to:
                          #https://careerkarma.com/blog/python-typeerror-string-indices-must-be-integers/
       # print(m)         # shows something without ['Name']
        
        #diagnostic = az('monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv" --query "value[?resourceGroup == `husdl-tu1-doccano`].name')
        
        #diagnostic = az('resource list --output json --query "[].{id:id, name:name}[? contains(id,`doccano`)]"')
        
        #diagnostic = az('monitor diagnostic-settings list --resource "/subscriptions/27b2a9c9-89f6-44cf-b4ec-74df29dce1fd/resourceGroups/husdl-tu1-doccano/providers/Microsoft.KeyVault/vaults/doccano-prod-kv"')
        #
        
        #todo: test variable inside command
        diagnostic = az(f'monitor diagnostic-settings list --resource {idm}')  #using variable in the middle of the command
#       print(f'Unsupported Azure resource type {​​o["type"]}​​')
        
        print (diagnostic, file=diagnostic_output)
else:
    print(logs)
###
# exit_code, members2, logs = az('ad group list --query "[].{Name:displayName, objectId:objectId}[? contains(Name,`TietoAllas`)]"')

# if exit_code == 0:
#      print(members2)

# else:
#      print(logs)
###

