$dataFactoryResourceGroupName="estu-df"
$dataFactoryName="eksote-prod-df"
$vmResourceGroupName="estu1-ws-eksote"
[string[]]$vmNames = "ws-eksote-vm-ir-ext-1"
$selfHostedIntegrationRuntimeName="IR-ext2"

Write-Host "Resource created. Fetching runtime key."

Write-Host $dataFactoryResourceGroupName
Write-Host $dataFactoryName
Write-Host $selfHostedIntegrationRuntimeName


$irRuntimeKey = (
    Get-AzDataFactoryV2IntegrationRuntimeKey -ResourceGroupName $dataFactoryResourceGroupName -DataFactoryName $dataFactoryName -Name $selfHostedIntegrationRuntimeName
)

Write-Host $irRuntimeKey.AuthKey1


Write-Host "Preparing VMs. This may take a while."
foreach($vmName in $vmNames) {
    Write-Host ("Installing gateway on machine {0}" -f $vmName)
    Invoke-AzVMRunCommand -ResourceGroupName $vmResourceGroupName -VMName $vmName -CommandId 'RunPowerShellScript' -ScriptPath 'gatewayInstall.ps1' -Parameter @{gatewayKey = $irRuntimeKey.AuthKey1}
    Write-Host "Done."
}

Write-Host "Setting up Integration Runtime finished."
